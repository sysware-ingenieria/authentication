package com.sysware.ingenieria.utils.exeptions;

/**
 * Se manejan los errores a nivel de aplicacion a nivel  negocio.
 */
public class BussinessException implements IException{
    private String message;

    public BussinessException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
