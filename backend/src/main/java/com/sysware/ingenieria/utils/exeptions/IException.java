package com.sysware.ingenieria.utils.exeptions;


//Permite crear un mensaje personalizado al error
public interface IException {
    public String getMessage();
}
