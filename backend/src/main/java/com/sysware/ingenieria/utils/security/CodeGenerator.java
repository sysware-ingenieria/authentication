package com.sysware.ingenieria.utils.security;


import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.Random;
import java.util.UUID;

import static com.sysware.ingenieria.utils.constans.K.SHA_1;
import static com.sysware.ingenieria.utils.security.PasswordGenerator.gerarHash;
import static com.sysware.ingenieria.utils.security.PasswordGenerator.passwordGenerator;
import static com.sysware.ingenieria.utils.security.PasswordGenerator.stringHexa;
import static com.sysware.ingenieria.utils.security.TokenGenerator.tokenGenerate;
import static com.sysware.ingenieria.utils.security.TokenGenerator.uuid;

public class CodeGenerator {

    private static double randomNumber() {
        return new Random().nextDouble();
    }

    public static Integer generarNumeroFactura(){
        Integer numeroFatura=null;
        try {

            SecureRandom prng = SecureRandom.getInstance("SHA1PRNG");

            //generate a random number
            String randomNum = new Integer(prng.nextInt()).toString();

            //get its digest
            MessageDigest sha = MessageDigest.getInstance("SHA-1");
            byte[] result =  sha.digest(randomNum.getBytes());
            numeroFatura=Math.abs(Integer.valueOf(randomNum));
            // System.out.println("Random number: " + numeroFatura);
            return numeroFatura;
        }catch (Exception e){

            return numeroFatura;
        }
    }


    //generamos un codigo de licencia a los usuarios para la ingenieria.
    public static String codeGenerate(int codeLength, boolean isDached) {
        String codeTrial = "";
        for (int i = 1; i <codeLength ; i++) {
            codeTrial=codeTrial+codeCharacterGenerate(randomNumber());
            if ( i%4==0 && i<codeLength && isDached){
                codeTrial=codeTrial+"-";
            }
        }
        return codeTrial;
    }

    public static Integer _uuid() {
        return Math.abs(Integer.valueOf( UUID.randomUUID().hashCode()));

    }




    public static char codeCharacterGenerate(double randomDouble){
        long ramdom36=(long) (randomDouble*36)+1;
        int modulator = 0;
        if (ramdom36>10){
            modulator=1;
        }
        long ascii36=47+ramdom36+(modulator*7);
        return (char) ascii36;
    }


    public static void main(String[] args) {
        System.out.println("GENARATE ->"+generarNumeroFactura());
        TokenGenerator tokenGenerator= new TokenGenerator();






        String frase = "lufgarciaqu,lunes2020";
        String frase2 = "lufgarciaqu,lunes2020 ";
        System.out.println(stringHexa(gerarHash(frase, "MD5")));
        System.out.println(passwordGenerator(frase, SHA_1).hashCode()==passwordGenerator(frase2, SHA_1).hashCode());
        System.out.println(stringHexa(gerarHash(frase, "SHA-256")));

        System.out.println(uuid());

        System.out.println(tokenGenerate(true));
        UUID x = UUID.randomUUID();
        String s = x.toString();
        Integer z= Math.abs(Integer.valueOf(s.hashCode()));
        System.out.println(_uuid());

    }
}
