package com.sysware.ingenieria.resources;


import com.sysware.ingenieria.businesses.UsuarioBusiness;
import com.sysware.ingenieria.entities.Directorio;
import com.sysware.ingenieria.entities.PersonaUsuario;
import com.sysware.ingenieria.entities.Usuario;
import com.sysware.ingenieria.representations.*;
import com.sysware.ingenieria.utils.exeptions.BussinessException;
import com.sysware.ingenieria.utils.exeptions.ExceptionResponse;
import com.sysware.ingenieria.utils.exeptions.IException;
import fj.data.Either;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/usuarios")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UsuarioResource {
    private UsuarioBusiness usuarioBusiness;

    public UsuarioResource(UsuarioBusiness usuarioBusiness) {
        this.usuarioBusiness = usuarioBusiness;
    }

    @GET
    public Response obtenerListaUsuario(@QueryParam("id_aplicacion") Long id_aplicacion,@QueryParam("id_usuario") Long id_usuario,
                                        @QueryParam("usuario") String usuario,
                                        @QueryParam("clave") String clave){
        Response response;

        Either<IException, List<Usuario>> allViewOffertsEither = usuarioBusiness.obtenerUsuarioApp(id_aplicacion,id_usuario, usuario,clave);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value().size());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @POST
    public Response crearUsuario(UsuarioDTO usuarioAppDTO) {
        Either<IException, Long> offerEither = usuarioBusiness.crearUsuarioApp( usuarioAppDTO);

        if(offerEither.isRight()) {
            return Response.status(Response.Status.OK).entity(offerEither.right().value()).build();
        } else {
            return ExceptionResponse.createErrorResponse(offerEither);
        }
    }

    @POST
    @Path("full")
    public Response crearUsuario(PersonaFullDatosDTO personaFullDatosDTO) {
        Either<IException, Long> offerEither = usuarioBusiness.crearUsuarioFULL( personaFullDatosDTO);

        if(offerEither.isRight()) {
            return Response.status(Response.Status.OK).entity(offerEither.right().value()).build();
        } else {
            return ExceptionResponse.createErrorResponse(offerEither);
        }
    }

    @PUT
    @Path("persona")
    public Response editarPersona(@QueryParam("uuid_usuario") Integer uuid_usuario,PersonaDTO personaDTO) {
        Either<IException, Long> offerEither = usuarioBusiness.editarPersona(uuid_usuario, personaDTO);

        if(offerEither.isRight()) {
            return Response.status(Response.Status.OK).entity(offerEither.right().value()).build();
        } else {
            return ExceptionResponse.createErrorResponse(offerEither);
        }
    }

    @PUT
    @Path("directorio")
    public Response editarDirectorio(@QueryParam("id_directorio") Long id_directorio,Directorio directorio) {
        Either<IException, Long> offerEither = usuarioBusiness.editarDirectorio(id_directorio, directorio);

        if(offerEither.isRight()) {
            return Response.status(Response.Status.OK).entity(offerEither.right().value()).build();
        } else {
            return ExceptionResponse.createErrorResponse(offerEither);
        }
    }

    @DELETE
    @Path("directorio")
    public Response eliminarDirectorio(@QueryParam("id_directorio") Long id_directorio,
                                       @QueryParam("id_mail") Long id_mail,
                                       @QueryParam("id_telefono") Long id_telefono) {
        Either<IException, Long> offerEither = usuarioBusiness.eliminarDirectorio(id_directorio, id_mail,id_telefono);

        if(offerEither.isRight()) {
            return Response.status(Response.Status.OK).entity(offerEither.right().value()).build();
        } else {
            return ExceptionResponse.createErrorResponse(offerEither);
        }
    }

    @DELETE
    @Path("persona")
    public Response eliminarPersona(@QueryParam("id_persona") Long id_persona,
                                       @QueryParam("uuid_usuario") Long uuid_usuario) {
        Either<IException, Long> offerEither = usuarioBusiness.eliminarPersona(id_persona, uuid_usuario);

        if(offerEither.isRight()) {
            return Response.status(Response.Status.OK).entity(offerEither.right().value()).build();
        } else {
            return ExceptionResponse.createErrorResponse(offerEither);
        }
    }
    @GET
    @Path("persona")
    public Response listaPersona(@QueryParam("uuid") List<Long> uuidList,@QueryParam("id_aplicacion") Long id_aplicacion,
                                 @QueryParam("usuario") String usuario,@QueryParam("clave") String clave,
                                 @QueryParam("id_persona") Long id_persona,
                                 @QueryParam("documento")String documento, @QueryParam("tipo_documento") String tipo_documento,
                                 @QueryParam("primer_nombre") String primer_nombre, @QueryParam("segundo_nombre") String segundo_nombre,
                                 @QueryParam("primer_apellido") String primer_apellido, @QueryParam("segundo_apellido") String segundo_apellido) {

        Either<IException, List<PersonaUsuario>> offerEither = usuarioBusiness.ListaPersona(uuidList,id_aplicacion,usuario,clave,id_persona,
                documento,tipo_documento,primer_nombre,segundo_nombre,primer_apellido,segundo_apellido);

        if(offerEither.isRight()) {
            return Response.status(Response.Status.OK).entity(offerEither.right().value()).build();
        } else {
            return ExceptionResponse.createErrorResponse(offerEither);
        }
    }

    @DELETE
    public Response eliminarUsuario(@QueryParam("uuid_usuario") Long uuid_usuario) {
        Either<IException, Long> offerEither = usuarioBusiness.eliminarUsuario( uuid_usuario);

        if(offerEither.isRight()) {
            return Response.status(Response.Status.OK).entity(offerEither.right().value()).build();
        } else {
            return ExceptionResponse.createErrorResponse(offerEither);
        }
    }

    @PUT
    public Response editarUsuario(@QueryParam("id_usuario") Integer _uuid,UsuarioDTO usuarioAppDTO) {
        Either<IException, Integer> offerEither = usuarioBusiness.editarUsuario( _uuid,usuarioAppDTO, false);

        if(offerEither.isRight()) {
            return Response.status(Response.Status.OK).entity(offerEither.right().value()).build();
        } else {
            return ExceptionResponse.createErrorResponse(offerEither);
        }
    }

    @PUT
    @Path("my-rol")
    public Response editarUsuarioCambiarRol(@QueryParam("id_usuario") Integer _uuid,UsuarioDTO usuarioAppDTO) {
        Either<IException, Integer> offerEither = usuarioBusiness.editarUsuario( _uuid,usuarioAppDTO,true);

        if(offerEither.isRight()) {
            return Response.status(Response.Status.OK).entity(offerEither.right().value()).build();
        } else {
            return ExceptionResponse.createErrorResponse(offerEither);
        }
    }

    @POST
    @Path("rol")
    public Response crearRolMenu(@QueryParam("id_usaurio")Integer _uuid,List<Long>id_rolUsuarioLista) {

        Either<IException, Long> offerEither = usuarioBusiness.crearUsuarioRoles(null, _uuid, id_rolUsuarioLista);

        if(offerEither.isRight()) {
            return Response.status(Response.Status.OK).entity(offerEither.right().value()).build();
        } else {
            return ExceptionResponse.createErrorResponse(offerEither);
        }
    }

    @GET
    @Path("rol")
    public Response obtenerIdRolesPorUsuario(@QueryParam("id_rol")Long id_rol,@QueryParam("id_usuario")Integer id_usuario) {

        Either<IException, List<Long>> offerEither = usuarioBusiness.obtenerIdRolesPorUsuario(null,id_rol,id_usuario);

        if(offerEither.isRight()) {
            return Response.status(Response.Status.OK).entity(offerEither.right().value()).build();

        } else {
            return ExceptionResponse.createErrorResponse(offerEither);
        }
    }

    @PUT
    @Path("rol")
    public Response borrarMenusPorRol(@QueryParam("id_usuario")Long id_usuario,List<Long>id_menuRolLista) {

        Either<IException, Integer> offerEither = usuarioBusiness.borrarRolesPorUsuario(id_usuario,id_menuRolLista);

        if(offerEither.isRight()) {
            return Response.status(Response.Status.OK).entity(offerEither.right().value()).build();
        } else {
            return ExceptionResponse.createErrorResponse(offerEither);
        }
    }



    @POST
    @Path("login")
    public Response iniciarSession(@QueryParam("id_aplicacion") Long id_aplicacion,@QueryParam("dispositivo") String dispositivo,AuthDTO authDTO){
        Response response;

        Either<IException, TokenDTO> iniarSesionEither = usuarioBusiness.iniarSesion(id_aplicacion,dispositivo,authDTO);


        if (iniarSesionEither.isRight()){

            response=Response.status(Response.Status.OK).entity(iniarSesionEither.right().value()).build();
        }else {
            if (iniarSesionEither.left().value().getClass().equals(BussinessException.class)){
                response=Response.status(Response.Status.OK).entity(iniarSesionEither.left().value().getMessage()).build();
            }else{
                response= ExceptionResponse.createErrorResponse(iniarSesionEither);
            }

        }
        return response;
    }
}
