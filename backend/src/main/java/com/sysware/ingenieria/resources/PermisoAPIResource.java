package com.sysware.ingenieria.resources;

import com.sysware.ingenieria.businesses.PermisoAPIBusiness;
import com.sysware.ingenieria.entities.PermisoAPI;
import com.sysware.ingenieria.representations.PermisoAPIDTO;
import com.sysware.ingenieria.utils.exeptions.ExceptionResponse;
import com.sysware.ingenieria.utils.exeptions.IException;
import fj.data.Either;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/apis")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PermisoAPIResource {
    private PermisoAPIBusiness permisoAPIBusiness;

    public PermisoAPIResource(PermisoAPIBusiness permisoAPIBusiness) {
        this.permisoAPIBusiness = permisoAPIBusiness;
    }



    @GET
    public Response obtenerListaRoles(@QueryParam("id_permiso_api")Long id_permiso_api, @QueryParam("id_aplicacion") Long id_aplicacion,
                                      @QueryParam("id_api") Long id_api,@QueryParam("http_verb") String http_verb,
                                      @QueryParam("http_uri") String http_uri){
        Response response;

        Either<IException, List<PermisoAPI>> allViewOffertsEither = permisoAPIBusiness.obtenerMenus(id_permiso_api,id_aplicacion,
                id_api, http_verb, http_uri);


        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value().size());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @POST
    public Response crearRol(@QueryParam("id_usuario")Long id_usuario, PermisoAPIDTO permisoAPIDTO) {
        Either<IException, Long> offerEither = permisoAPIBusiness.crearPermisoAPI(id_usuario,permisoAPIDTO);
        if(offerEither.isRight()) {
            return Response.status(Response.Status.OK).entity(offerEither.right().value()).build();
        } else {
            return ExceptionResponse.createErrorResponse(offerEither);
        }
    }



}
