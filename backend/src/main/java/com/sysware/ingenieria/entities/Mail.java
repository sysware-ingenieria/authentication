package com.sysware.ingenieria.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by luis on 3/05/17.
 */
public class Mail {
    private Long id_mail;
    private String mail;
    private String tipo;
    private Integer preferencia;

    @JsonCreator
    public Mail(@JsonProperty("id_mail") Long id_mail,@JsonProperty("mail") String mail,
                @JsonProperty("tipo") String tipo,
                   @JsonProperty("preferencia") Integer preferencia) {
        this.id_mail=id_mail;
        this.mail = mail;
        this.tipo = tipo;
        this.preferencia = preferencia;
    }

    public Long getId_mail() {
        return id_mail;
    }

    public void setId_mail(Long id_mail) {
        this.id_mail = id_mail;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Integer getPreferencia() {
        return preferencia;
    }

    public void setPreferencia(Integer preferencia) {
        this.preferencia = preferencia;
    }
}
