package com.sysware.ingenieria.entities;

/**
 * Created by luis on 3/05/17.
 */
public class PermisoAPI {
    private Long id_permiso_api;
    private Long   id_aplicacion;
    private Long   id_api;
    private String http_verb;
    private String http_uri;

    public PermisoAPI(Long id_permiso_api, Long id_aplicacion, Long id_api, String http_verb, String http_uri) {
        this.id_permiso_api = id_permiso_api;
        this.id_aplicacion = id_aplicacion;
        this.id_api = id_api;
        this.http_verb = http_verb;
        this.http_uri = http_uri;
    }

    public Long getId_permiso_api() {
        return id_permiso_api;
    }

    public void setId_permiso_api(Long id_permiso_api) {
        this.id_permiso_api = id_permiso_api;
    }

    public Long getId_aplicacion() {
        return id_aplicacion;
    }

    public void setId_aplicacion(Long id_aplicacion) {
        this.id_aplicacion = id_aplicacion;
    }

    public Long getId_api() {
        return id_api;
    }

    public void setId_api(Long id_api) {
        this.id_api = id_api;
    }

    public String getHttp_verb() {
        return http_verb;
    }

    public void setHttp_verb(String http_verb) {
        this.http_verb = http_verb;
    }

    public String getHttp_uri() {
        return http_uri;
    }

    public void setHttp_uri(String http_uri) {
        this.http_uri = http_uri;
    }
}
