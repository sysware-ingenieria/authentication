package com.sysware.ingenieria.entities;

/**
 * Created by luis on 3/05/17.
 */
public class Menus {

    private Long id_menu;
    private Long id_aplicacion;
    private String nombre;
    private Integer tipo;
    private String ruta;
    private String icono;
    private String avatar;
    private String background;
    private Integer estado;
    private String dispositivo;
    private Long id_menu_padre;

    public Menus(Long id_menu, Long id_aplicacion, String nombre, Integer tipo, String ruta, String icono, String avatar, String background, Integer estado, String dispositivo, Long id_menu_padre) {
        this.id_menu = id_menu;
        this.id_aplicacion = id_aplicacion;
        this.nombre = nombre;
        this.tipo = tipo;
        this.ruta = ruta;
        this.icono = icono;
        this.avatar = avatar;
        this.background = background;
        this.estado = estado;
        this.dispositivo = dispositivo;
        this.id_menu_padre = id_menu_padre;
    }

    public Long getId_menu_padre() {
        return id_menu_padre;
    }

    public void setId_menu_padre(Long id_menu_padre) {
        this.id_menu_padre = id_menu_padre;
    }

    public String getDispositivo() {
        return dispositivo;
    }

    public void setDispositivo(String dispositivo) {
        this.dispositivo = dispositivo;
    }

    public Long getId_menu() {
        return id_menu;
    }

    public void setId_menu(Long id_menu) {
        this.id_menu = id_menu;
    }

    public Long getId_aplicacion() {
        return id_aplicacion;
    }

    public void setId_aplicacion(Long id_aplicacion) {
        this.id_aplicacion = id_aplicacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public String getIcono() {
        return icono;
    }

    public void setIcono(String icono) {
        this.icono = icono;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }
}
