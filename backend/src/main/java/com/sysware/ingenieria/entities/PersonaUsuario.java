package com.sysware.ingenieria.entities;

/**
 * Created by luis on 1/07/17.
 */
public class PersonaUsuario {
    private Persona persona;
    private Usuario usuario;
    private Directorio directorio;

    public PersonaUsuario(Persona persona, Usuario usuario, Directorio directorio) {
        this.persona = persona;
        this.usuario = usuario;
        this.directorio = directorio;
    }

    public Directorio getDirectorio() {
        return directorio;
    }

    public void setDirectorio(Directorio directorio) {
        this.directorio = directorio;
    }



    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}
