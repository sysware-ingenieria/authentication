package com.sysware.ingenieria.DAOs;


import com.sysware.ingenieria.entities.UsuarioApp;
import com.sysware.ingenieria.mappers.UsuarioAppMapper;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;
@RegisterMapper(UsuarioAppMapper.class)
public interface UsuarioAppDAO {

    @SqlQuery("SELECT ID_USER_APP FROM USER_APP WHERE ID_USER_APP IN (SELECT MAX( ID_USER_APP ) FROM USER_APP) ")
    Long getPkLast();

    @SqlQuery("\n" +
            "SELECT * FROM USER_APP  US_AP\n" +
            "  WHERE  (US_AP.ID_USER_APP=:id_usuario_app or :id_usuario_app IS NULL ) AND\n" +
            "         (US_AP.USER_APP=:usuario or :usuario IS NULL ) AND\n" +
            "         (US_AP.PASSWORD=:clave or :clave IS NULL )")
    List<UsuarioApp> OBTENER_USUARIO(@Bind("id_usuario_app") Long id_usuario_app,@Bind("usuario") String usuario,@Bind("clave") String clave);

    @SqlQuery("SELECT * FROM USER_APP  US_AP " +
            "  WHERE  "+
            "         (US_AP.USER_APP=:usuario )")
    List<UsuarioApp>  OBTENER_USUARIO_POR_NOMBRE(@Bind("usuario") String usuario);

    @SqlQuery("SELECT * FROM USER_APP US_AP " +
            "  WHERE  "+
            "         (US_AP.PASSWORD=:clave )")
    List<UsuarioApp>  OBTENER_USUARIO_POR_CLAVE(@Bind("clave") String clave);

    @SqlQuery("SELECT *  FROM USER_APP  US_AP " +
            "  WHERE   " +
            "         (US_AP.USER_APP=:usuario) AND " +
            "         (US_AP.PASSWORD=:clave)")
    List<UsuarioApp>  VEFICAR_CREDENCIALES(@Bind("usuario") String usuario,@Bind("clave") String clave);


    @SqlUpdate("INSERT INTO USER_APP ( USER_APP, PASSWORD) VALUES (:usuario, :clave)")
    long CREAR_USUARIOAPP(@Bind("usuario") String usuario, @Bind("clave") String clave);
}
