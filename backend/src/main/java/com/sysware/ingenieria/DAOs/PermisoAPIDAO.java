package com.sysware.ingenieria.DAOs;

import com.sysware.ingenieria.entities.PermisoAPI;
import com.sysware.ingenieria.mappers.PermisoAPIMapper;
import com.sysware.ingenieria.representations.PermisoAPIDTO;
import org.skife.jdbi.v2.sqlobject.*;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;


@RegisterMapper(PermisoAPIMapper.class)
public interface PermisoAPIDAO {

    @SqlQuery("SELECT ID_PERMISO_API FROM PERMISOS_API WHERE ID_PERMISO_API IN (SELECT MAX( ID_PERMISO_API ) FROM PERMISOS_API)\n ")
    Long getPkLast();

    @SqlUpdate("INSERT INTO permisos_api ( id_aplicacion, id_api, http_verb, http_uri) VALUES (:perAPI.id_aplicacion, :perAPI.id_api, :perAPI.http_verb, :perAPI.http_uri)")
    long CREAR_PERMISO_API(@Bind("id_usuario") Long id_usuario, @BindBean("perAPI") PermisoAPIDTO permisoAPIDTO);


    @SqlQuery(" SELECT  * FROM permisos_api   pe_API  WHERE"+
            " (pe_API.id_permiso_api=:id_permiso_api OR :id_permiso_api IS NULL ) AND"+
            " (pe_API.id_aplicacion=:id_aplicacion OR :id_aplicacion IS NULL ) AND"+
            " (pe_API.id_api=:id_api OR :id_api IS NULL ) AND"+
            " (pe_API.http_verb=:http_verb OR :http_verb IS NULL ) AND"+
            " (pe_API.http_uri LIKE :http_uri OR :http_uri IS NULL )")
    List<PermisoAPI> OBTENER_PERMISO_API(@Bind("id_permiso_api")Long id_permiso_api, @Bind("id_aplicacion") Long id_aplicacion,
                                         @Bind("id_api") Long id_api,@Bind("http_verb") String http_verb,
                                         @Bind("http_uri") String http_uri);



    @SqlQuery("    SELECT count(id_permiso_api) FROM permisos_api  pe_API   WHERE  (pe_API.id_aplicacion=:id_aplicacion AND  pe_API.http_uri=:http_uri AND  pe_API.http_verb=:http_verb)")
    Integer VERIFICAR_PERMISO_API(@Bind("id_aplicacion") Long id_usuario_app,@Bind("http_uri") String http_uri,@Bind("http_verb") String http_verb);
}



