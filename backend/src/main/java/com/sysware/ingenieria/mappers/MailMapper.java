package com.sysware.ingenieria.mappers;

import com.sysware.ingenieria.entities.Mail;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by luis on 4/07/17.
 */
public class MailMapper implements ResultSetMapper<Mail> {
    @Override
    public Mail map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Mail(
                resultSet.getLong("id_mail"),
                resultSet.getString("mail"),
                resultSet.getString("tipo"),
                resultSet.getInt("preferencia")
        );
    }
}
