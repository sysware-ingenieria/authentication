package com.sysware.ingenieria.mappers;


import com.sysware.ingenieria.entities.Dato;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DatoMapper implements ResultSetMapper<Dato> {

    @Override
    public Dato map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Dato(
                resultSet.getLong("id_dato"),
                resultSet.getString("nombre"),
                resultSet.getString("descripcion")
        );
    }
}
