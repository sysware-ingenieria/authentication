package com.sysware.ingenieria.mappers;

import com.sysware.ingenieria.entities.Rol;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


public class RolMapper implements ResultSetMapper<Rol> {

    @Override
    public Rol map(int index, ResultSet resultSet, StatementContext ctx) throws SQLException {
        return new Rol(
                resultSet.getLong("id_rol"),
                resultSet.getLong("id_aplicacion"),
                resultSet.getString("rol"),
                resultSet.getString("descripcion")
        );
    }
}
