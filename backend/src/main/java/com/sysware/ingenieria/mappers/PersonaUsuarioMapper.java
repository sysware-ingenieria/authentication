package com.sysware.ingenieria.mappers;

import com.sysware.ingenieria.entities.Directorio;
import com.sysware.ingenieria.entities.Persona;
import com.sysware.ingenieria.entities.PersonaUsuario;
import com.sysware.ingenieria.entities.Usuario;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by luis on 1/07/17.
 */
public class PersonaUsuarioMapper implements ResultSetMapper<PersonaUsuario> {
    @Override
    public PersonaUsuario map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new PersonaUsuario(
                new Persona(
                        resultSet.getLong("id_persona"),
                        resultSet.getInt("uuid"),
                        resultSet.getString("documento"),
                        resultSet.getString("tipo_documento"),
                        resultSet.getString("primer_nombre"),
                        resultSet.getString("segundo_nombre"),
                        resultSet.getString("primer_apellido"),
                        resultSet.getString("segundo_apellido")
                ),
                new Usuario(
                        resultSet.getInt("uuid"),
                        resultSet.getLong("id_aplicacion"),
                        resultSet.getString("usuario"),
                        resultSet.getString("clave"),
                        null
                ),
                new Directorio(null, null,null)
        );
    }
}
