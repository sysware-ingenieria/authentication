package com.sysware.ingenieria.mappers;

import com.sysware.ingenieria.entities.UsuarioApp;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by luis on 3/05/17.
 */
public class UsuarioAppMapper implements ResultSetMapper<UsuarioApp>{

    @Override
    public UsuarioApp map(int index, ResultSet resultSet, StatementContext ctx) throws SQLException {
        return new UsuarioApp(
                resultSet.getLong("ID_USER_APP"),
                resultSet.getString("USER_APP"),
                resultSet.getString("PASSWORD")
        );
    }
}
