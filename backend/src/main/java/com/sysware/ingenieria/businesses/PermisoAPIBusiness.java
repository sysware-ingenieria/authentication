package com.sysware.ingenieria.businesses;

import com.sysware.ingenieria.DAOs.PermisoAPIDAO;
import com.sysware.ingenieria.entities.PermisoAPI;
import com.sysware.ingenieria.representations.PermisoAPIDTO;
import com.sysware.ingenieria.utils.exeptions.BussinessException;
import com.sysware.ingenieria.utils.exeptions.IException;
import com.sysware.ingenieria.utils.exeptions.ManagementException;
import com.sysware.ingenieria.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.List;

import static com.sysware.ingenieria.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.sysware.ingenieria.utils.constans.K.messages_error;
import static com.sysware.ingenieria.utils.constans.K.messages_errors;
import static com.sysware.ingenieria.utils.filters.FormatoQuerySqlFilter.formatoLIKESql;
import static com.sysware.ingenieria.utils.filters.FormatoQuerySqlFilter.formatoLongSql;
import static com.sysware.ingenieria.utils.filters.FormatoQuerySqlFilter.formatoStringSql;

/**
 * Created by luis on 4/05/17.
 */
public class PermisoAPIBusiness {

    private PermisoAPIDAO permisoAPIDAO;

    public PermisoAPIBusiness(PermisoAPIDAO permisoAPIDAO) {
        this.permisoAPIDAO = permisoAPIDAO;
    }

    public Either<IException,List<PermisoAPI>> obtenerMenus(Long id_permiso_api,Long id_aplicacion,
                                                             Long id_api,String http_verb,
                                                            String http_uri){
        try {
            List<PermisoAPI> aplicacionList = permisoAPIDAO.OBTENER_PERMISO_API(formatoLongSql(id_permiso_api),formatoLongSql(id_aplicacion),formatoLongSql(id_api),
                    formatoLIKESql(http_uri),formatoStringSql(http_verb));
            return Either.right(aplicacionList);

        }catch (Exception e){
            e.printStackTrace();
            return Either.left(ManagementException.catchException(e));
        }
    }

    public Either<IException, Long> crearPermisoAPI(Long id_usuario, PermisoAPIDTO permisoAPIDTO) {
        List<String> msn=new ArrayList<>();
        try {

            if (formatoLongSql(permisoAPIDTO.getId_aplicacion())==null){
                msn.add("La Aplicacion no se reconoce, para registrar URI");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
            if (verificarAPIURI(permisoAPIDTO.getId_aplicacion(),permisoAPIDTO.getHttp_uri(),permisoAPIDTO.getHttp_verb())){
                msn.add("L URI ["+permisoAPIDTO.getHttp_uri() +" VERBO"+ permisoAPIDTO.getHttp_verb()+"] ya existe");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
            msn.add("OK");
            System.out.println("            Inicia a crear URI    ***********************************");
            if (permisoAPIDTO!=null){

                long id_rol = permisoAPIDAO.CREAR_PERMISO_API(formatoLongSql(id_usuario), permisoAPIDTO);

                if (id_rol>0){

                    return Either.right(permisoAPIDAO.getPkLast());


                }else{
                    msn.add("No se reconoce formato del Menu , esta mal formado");

                }

            }
            return Either.left(new BussinessException(messages_errors(msn)));
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    private boolean verificarAPIURI(Long id_aplicacion, String http_uri, String http_verb) {
        return (permisoAPIDAO.VERIFICAR_PERMISO_API(id_aplicacion,http_uri,http_verb)==0)?false:true;
    }


}
