package com.sysware.ingenieria.businesses;

import com.sysware.ingenieria.DAOs.RolDAO;
import com.sysware.ingenieria.entities.Rol;
import com.sysware.ingenieria.representations.RolDTO;
import com.sysware.ingenieria.utils.exeptions.BussinessException;
import com.sysware.ingenieria.utils.exeptions.IException;
import com.sysware.ingenieria.utils.exeptions.ManagementException;
import com.sysware.ingenieria.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static com.sysware.ingenieria.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.sysware.ingenieria.utils.constans.K.messages_error;
import static com.sysware.ingenieria.utils.constans.K.messages_errors;
import static com.sysware.ingenieria.utils.filters.FormatoQuerySqlFilter.formatoLIKESql;
import static com.sysware.ingenieria.utils.filters.FormatoQuerySqlFilter.formatoLongSql;


public class RolBusiness {
    private RolDAO rolDAO;

    public RolBusiness(RolDAO rolDAO) {
        this.rolDAO = rolDAO;
    }

    public Either<IException,List<Rol>> obtenerRoles(Long id_applicacion, Long id_rol, String rol, String des){
        try {
            List<Rol> aplicacionList = rolDAO.OBTENER_ROL(formatoLongSql(id_applicacion),formatoLongSql(id_rol),formatoLIKESql( des));
            return Either.right(aplicacionList);

        }catch (Exception e){
            e.printStackTrace();
            return Either.left(ManagementException.catchException(e));
        }
    }
    public Either<IException,List<Rol>> obtenerRolesByUUID(Long id_applicacion, Long id_rol, String rol, String des,
                                                           Long id_usuario_rol,



    Long id_usuario,
                                                           Long uuid){
        try {
            List<Rol> aplicacionList = rolDAO.OBTENER_ROL_COMPLETO_BY_UUID(formatoLongSql(id_applicacion),formatoLongSql(id_rol),formatoLIKESql( rol),
                    formatoLongSql(id_usuario_rol),
                    formatoLongSql(id_usuario),
                    formatoLongSql(uuid));
            return Either.right(aplicacionList);

        }catch (Exception e){
            e.printStackTrace();
            return Either.left(ManagementException.catchException(e));
        }
    }





    public Either<IException, Long> crearRol( RolDTO rolDTO) {
        List<String> msn=new ArrayList<>();
        try {

            if (formatoLongSql(rolDTO.getId_aplicacion())==null){
                msn.add("La Aplicacion creador de aplicacion no se reconoce");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
            if (verificarApplicacionUsuario(rolDTO.getId_aplicacion(),rolDTO.getRol())){
                msn.add("La aplicacion ["+rolDTO.getRol()+"] ya existe");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
            msn.add("OK");
            System.out.println("            Inicia a crear Rol    ***********************************");
            if (rolDTO!=null){

                long id_rol = rolDAO.CREAR_ROL(formatoLongSql(rolDTO.getId_aplicacion()), rolDTO.getRol(),rolDTO.getDescripcion());
                return Either.right(rolDAO.getPkLast());


            }
            return Either.left(new BussinessException(messages_errors(msn)));
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    private boolean verificarApplicacionUsuario(Long id_aplicacion, String nombre) {
        return (rolDAO.VERIFICAR_ROL(id_aplicacion,nombre)==0)?false:true;
    }

    public Either<IException, Long> crearRolMenu(Long id_rol, List<Long> id_menuLista) {
        List<String> msn=new ArrayList<>();
        List<Long> id_menuListaFinal=new ArrayList<>();
        try {

            if (formatoLongSql(id_rol)==null || id_menuLista.size()<=0){
                msn.add("Fatan datos  id rol y/o id menu");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
            for (Long id_menu:id_menuLista ){
                if (verificarRolMenu(id_rol,id_menu)){
                    msn.add("El menu "+id_menu+" ya existe");

                }else{
                    id_menuListaFinal.add(id_menu);
                }
            }
            if (id_menuListaFinal.size()==0){
                return Either.left(new BussinessException(messages_errors(msn)));
            }
            msn.add("OK");
            System.out.println("            Inicia a crear Rol    ***********************************");

            long id_rol_menu=0;
            for (Long id_menu:id_menuListaFinal ){
                 id_rol_menu = rolDAO.CREAR_ROL_MENU(formatoLongSql(id_rol),formatoLongSql(id_menu) );
            }


            return Either.right(rolDAO.getPkLast_ROLES_MENU());


        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    private boolean verificarRolMenu(Long id_rol, Long id_menu) {
        return (rolDAO.VERIFICAR_ROL_MENU(id_rol,id_menu)==0)?false:true;
    }

    public Either<IException, Long> crearPermiso(Long id_rol, Long id_permiso_api) {
        List<String> msn=new ArrayList<>();
        try {

            if (formatoLongSql(id_rol)==null || formatoLongSql(id_permiso_api)==null){
                msn.add("Faltan datos  id rol y/o id permiso api");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
            if (verificarRolPermiso(id_rol,id_permiso_api)){
                msn.add("El menu  ya existe");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
            msn.add("OK");
            System.out.println("            Inicia a crear Permiso    ***********************************");


            long id_rol_menu = rolDAO.CREAR_ROL_PERMISO_API(formatoLongSql(id_rol),formatoLongSql(id_permiso_api) );

            return Either.right(rolDAO.getPkLast_ROL_PERMISO_API());


        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }

    }

    private boolean verificarRolPermiso(Long id_rol, Long id_permiso_api) {
        return (rolDAO.VERIFICAR_ROL_PERMISO_API(id_rol,id_permiso_api)==0)?false:true;
    }

    public Either<IException, List<Long>> obtenerIdMenusPorRol(Long id_rol, Long id_menu) {

        List<Long> idMenus_Lista= new ArrayList<>();
        List<String> msn=new ArrayList<>();

        try {
            if (formatoLongSql(id_rol)==null && formatoLongSql(id_menu)==null){
                msn.add("Faltan datos  id rol y id menu ");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
            idMenus_Lista=rolDAO.OBTENER_MENU_ROL(formatoLongSql(id_menu),formatoLongSql(id_rol),formatoLongSql(id_menu));
            return Either.right(idMenus_Lista);

        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }

    }

    public Either<IException, Integer> borrarMenusPorRol(Long id_rol, List<Long> id_menuLista) {

        List<Integer> idMenus_ListaFinal= new ArrayList<>();
        List<String> msn=new ArrayList<>();

        try {
            if (formatoLongSql(id_rol)==null){
                msn.add("Faltan datos  id rol  ");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
            if (id_menuLista.size()>0){
                for (Long id_menu:id_menuLista)
                    idMenus_ListaFinal.add(rolDAO.ELIMINAR_MENU_ROL(null,formatoLongSql(id_rol),formatoLongSql(id_menu)));

                return Either.right(id_menuLista.size());
            }else{
                msn.add("Faltan datos  id menu ");
                return Either.left(new BussinessException(messages_errors(msn)));
            }


        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


}
