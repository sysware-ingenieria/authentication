package com.sysware.ingenieria.representations;

import com.sysware.ingenieria.entities.Menus;
import com.sysware.ingenieria.entities.Rol;

import java.awt.*;
import java.util.List;

/**
 * Created by luis on 15/05/17.
 */
public class TokenDTO {
    private Long id_usuario;
    private Long id_aplicacion;
    private String usuario;

    private List<Menus> menus;
    private List<Rol> roles;

    public TokenDTO(Long id_usuario, Long id_aplicacion, String usuario, List<Menus> menus, List<Rol> roles) {
        this.id_usuario = id_usuario;
        this.id_aplicacion = id_aplicacion;
        this.usuario = usuario;
        this.menus = menus;
        this.roles = roles;
    }

    public Long getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(Long id_usuario) {
        this.id_usuario = id_usuario;
    }

    public Long getId_aplicacion() {
        return id_aplicacion;
    }

    public void setId_aplicacion(Long id_aplicacion) {
        this.id_aplicacion = id_aplicacion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public List<Menus> getMenus() {
        return menus;
    }

    public void setMenus(List<Menus> menus) {
        this.menus = menus;
    }

    public List<Rol> getRoles() {
        return roles;
    }

    public void setRols(List<Rol> roles) {
        this.roles = roles;
    }
}
