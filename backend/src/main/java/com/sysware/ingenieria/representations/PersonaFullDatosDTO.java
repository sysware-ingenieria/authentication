package com.sysware.ingenieria.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.sysware.ingenieria.entities.Directorio;
import com.sysware.ingenieria.entities.Persona;
import com.sysware.ingenieria.entities.Usuario;

/**
 * Created by luis on 30/06/17.
 */
public class PersonaFullDatosDTO {
    private PersonaDTO personaDTO;
    private DirectorioDTO directorioDTO;
    private UsuarioDTO usuarioDTO;

    @JsonCreator
    public PersonaFullDatosDTO(@JsonProperty("persona") PersonaDTO personaDTO,
                               @JsonProperty("directorio") DirectorioDTO directorioDTO,
                               @JsonProperty("usuario") UsuarioDTO usuarioDTO) {
        this.personaDTO = personaDTO;
        this.directorioDTO = directorioDTO;
        this.usuarioDTO = usuarioDTO;
    }

    public PersonaDTO getPersonaDTO() {
        return personaDTO;
    }

    public void setPersonaDTO(PersonaDTO personaDTO) {
        this.personaDTO = personaDTO;
    }

    public DirectorioDTO getDirectorioDTO() {
        return directorioDTO;
    }

    public void setDirectorioDTO(DirectorioDTO directorioDTO) {
        this.directorioDTO = directorioDTO;
    }

    public UsuarioDTO getUsuarioDTO() {
        return usuarioDTO;
    }

    public void setUsuarioDTO(UsuarioDTO usuarioDTO) {
        this.usuarioDTO = usuarioDTO;
    }
}
