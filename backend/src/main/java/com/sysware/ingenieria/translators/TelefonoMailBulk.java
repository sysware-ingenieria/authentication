package com.sysware.ingenieria.translators;

import com.sysware.ingenieria.entities.Telefono;
import com.sysware.ingenieria.representations.MailDTO;
import com.sysware.ingenieria.representations.TelefonoDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by luis on 30/06/17.
 */
public class TelefonoMailBulk {
    public static List<String> mailTelList;
    public static List<String> tipoList;
    public static List<Integer> preferenciaList;


    public TelefonoMailBulk() {
    }

    public TelefonoMailBulk(List<String> mailTelList, List<String> tipoList, List<Integer> preferenciaList) {
        this.mailTelList = mailTelList;
        this.tipoList = tipoList;
        this.preferenciaList = preferenciaList;
    }

    public static TelefonoMailBulk traductorTelefono(List<TelefonoDTO> telefonoDTOList){
        mailTelList= new ArrayList<>();
        tipoList= new ArrayList<>();
        preferenciaList= new ArrayList<>();
        for (TelefonoDTO dto: telefonoDTOList){
            mailTelList.add(dto.getNumero());
            tipoList.add(dto.getTipo());
            preferenciaList.add(dto.getPreferencia());
        }
        return new TelefonoMailBulk(mailTelList,tipoList,preferenciaList);
    }

    public static TelefonoMailBulk traductorMails(List<MailDTO> mailDTOList){
        mailTelList= new ArrayList<>();
        tipoList= new ArrayList<>();
        preferenciaList= new ArrayList<>();
        for (MailDTO dto: mailDTOList){
            mailTelList.add(dto.getMail());
            tipoList.add(dto.getTipo());
            preferenciaList.add(dto.getPreferencia());
        }

        return new TelefonoMailBulk(mailTelList,tipoList,preferenciaList);
    }


    public List<String> getMailTelList() {
        return mailTelList;
    }

    public void setMailTelList(List<String> mailTelList) {
        this.mailTelList = mailTelList;
    }

    public List<String> getTipoList() {
        return tipoList;
    }

    public void setTipoList(List<String> tipoList) {
        this.tipoList = tipoList;
    }

    public List<Integer> getPreferenciaList() {
        return preferenciaList;
    }

    public void setPreferenciaList(List<Integer> preferenciaList) {
        this.preferenciaList = preferenciaList;
    }
}
