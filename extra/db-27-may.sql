CREATE DATABASE  IF NOT EXISTS `auth` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `auth`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: auth
-- ------------------------------------------------------
-- Server version	5.7.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `aplicaciones`
--

LOCK TABLES `aplicaciones` WRITE;
/*!40000 ALTER TABLE `aplicaciones` DISABLE KEYS */;
INSERT INTO `aplicaciones` VALUES (1,1,'Ofertas','Tiendas y surtidos','v1','2017-05-16 10:17:51','2017-05-16 15:17:51','BZzaSyDDvpCczVBNx_YFb7mpxLOON2g6XKtO4Cw','dt5fR_Mtm9'),(2,2,'AppRuta','Administracion rutas de colegios','r1.0','2017-05-16 11:07:06','2017-05-16 16:07:06','BZzaSyDDvpCczVBNx_YFb7mpxLOON2g6XKtO4Cw','dt5fR_Mtm9'),(3,4,'Ofertas','Ofertas','v1.0','2017-05-18 15:09:52','2017-05-18 20:09:52','BZzaSyDDvpCczVBNx_YFb7mpxLOON2g6XKtO4Cw','dt5fR_Mtm9');
/*!40000 ALTER TABLE `aplicaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `categorias`
--

LOCK TABLES `categorias` WRITE;
/*!40000 ALTER TABLE `categorias` DISABLE KEYS */;
INSERT INTO `categorias` VALUES (1,11),(2,12),(3,13),(4,14),(5,15),(6,16),(7,17),(8,18),(9,19),(10,20),(11,21);
/*!40000 ALTER TABLE `categorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `clientes`
--

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` VALUES (1,2,'Caracas Sur');
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `datos`
--

LOCK TABLES `datos` WRITE;
/*!40000 ALTER TABLE `datos` DISABLE KEYS */;
INSERT INTO `datos` VALUES (1,'coca-cola',NULL),(2,'Colgate',NULL),(3,'Maggi',NULL),(4,'Lifebuoy',NULL),(5,'Nescafe',NULL),(6,'Pepsi',NULL),(7,'Lay\'s',NULL),(8,'Knorr',NULL),(9,'Dove',NULL),(10,'Tide',NULL),(11,'Carnes y Embutidos',NULL),(12,'Frutas y Verduras',NULL),(13,'Huevos, Lácteos y Café.',NULL),(14,'Aceite, Pasta y Legumbres',NULL),(15,'Zumos y Bebidas',NULL),(16,'Conservas y Comida Preparada. ',NULL),(17,'Aperitivos',NULL),(18,' Algas, Tofu y Preparados.',NULL),(19,'Infantil',NULL),(20,'Cosmética y Cuidado Personal.',NULL),(21,'Hogar y Limpieza.  ',NULL),(22,'ARROZ',NULL),(23,'HARINA DE MAÍZ Y OTRAS HARINAS',NULL),(24,'PASTAS SECOS',NULL),(25,'CEREALES PREPARADOS',NULL),(26,'CEREALES PARA SOPA',NULL),(27,'PAN',NULL),(28,'OTROS PRODUCTOS DE PANADERÍA',NULL),(29,'PAPA',NULL),(30,'YUCA',NULL),(31,'OTROS TUBÉRCULOS',NULL),(32,'PLÁTANO',NULL),(33,'CEBOLLA',NULL),(34,'TOMATE',NULL),(35,'ZANAHORIA',NULL),(36,'OTRAS HORTALIZAS Y LEGUMBRES FRESCAS',NULL),(37,'FRÍJOL',NULL),(38,'ARVEJA',NULL),(39,'OTRAS HORTALIZAS Y LEGUMBRES SECAS',NULL),(40,'OTRAS HORTALIZAS Y LEGUMBRES ENLATADAS',NULL),(41,'NARANJAS',NULL),(42,'BANANOS',NULL),(43,'TOMATE DE ÁRBOL',NULL),(44,'MORAS',NULL),(45,'OTRAS FRUTAS FRESCAS',NULL),(46,'FRUTAS EN CONSERVA',NULL),(47,'CARNE DE RES',NULL),(48,'CARNE DE CERDO',NULL),(49,'CARNE DE POLLO',NULL),(50,'CARNES FRÍAS Y EMBUTIDOS',NULL),(51,'PESCADO DE MAR, RÍO Y ENLATADO',NULL),(52,'OTRAS DE MAR',NULL),(53,'HUEVOS',NULL),(54,'LECHE',NULL),(55,'QUESO',NULL),(56,'OTROS DERIVADOS LÁCTEOS',NULL),(57,'ACEITES',NULL),(58,'GRASAS',NULL),(59,'PANELA',NULL),(60,'AZÚCAR',NULL),(61,'CAFÉ',NULL),(62,'CHOCOLATE',NULL),(63,'SAL',NULL),(64,'OTROS CONDIMENTOS',NULL),(65,'SOPAS Y CREMAS',NULL),(66,'SALSA Y MAYONESA',NULL),(67,'OTROS ABARROTES',NULL),(68,'JUGOS',NULL),(69,'GASEOSAS Y MALTAS',NULL),(70,'OTRAS BEBIDAS  NO ALCOHÓLICAS',NULL),(71,'ALMUERZO',NULL),(72,'HAMBURGUESA',NULL),(73,'COMIDAS RÁPIDAS CALIENTES',NULL),(74,'GASTOS DE CAFETERÍA',NULL),(75,'COMIDAS RÁPIDAS FRÍAS',NULL),(76,'ARRENDAMIENTO EFECTIVO',NULL),(77,'ARRENDAMIENTO IMPUTADO',NULL),(78,'SERVICIO DOMÉSTICO',NULL),(79,'GAS',NULL),(80,'ENERGÍA ELÉCTRICA',NULL),(81,'ACUEDUCTO, ALCANTARILLADO Y ASEO',NULL),(82,'MUEBLES DE SALA',NULL),(83,'MUEBLES DE COMEDOR',NULL),(84,'MUEBLES DE ALCOBA',NULL),(85,'OTROS MUEBLES DEL HOGAR',NULL),(86,'NEVERA',NULL),(87,'ESTUFA',NULL),(88,'LAVADORA',NULL),(89,'OTROS APARATOS DEL HOGAR',NULL),(90,'OLLAS',NULL),(91,'SARTENES Y REFRACTARIAS',NULL),(92,'VAJILLA',NULL),(93,'CUBIERTOS',NULL),(94,'OTROS UTENSILIOS DOMÉSTICOS',NULL),(95,'JUEGO DE SÁBANAS Y FUNDAS',NULL),(96,'COBIJAS Y CUBRELECHOS',NULL),(97,'COLCHONES Y ALMOHADAS',NULL),(98,'CORTINAS',NULL),(99,'TOALLAS Y MANTELES',NULL),(100,'JABONES',NULL),(101,'DETERGENTES Y BLANQUEADORES',NULL),(102,'LIMPIADORES Y DESINFECTANTES',NULL),(103,'INSECTICIDAS',NULL),(104,'CERAS',NULL),(105,'PAPELES DE COCINA',NULL),(106,'OTROS UTENSILIOS DE ASEO',NULL),(107,'CAMISAS PARA HOMBRE',NULL),(108,'PANTALONES PARA HOMBRE',NULL),(109,'ROPA INTERIOR PARA HOMBRE',NULL),(110,'OTRAS PRENDAS DE VESTIR PARA HOMBRE',NULL),(111,'BLUSA PARA MUJER',NULL),(112,'PANTALONES PARA MUJER',NULL),(113,'ROPA INTERIOR PARA MUJER',NULL),(114,'OTRAS PRENDAS DE VESTIR PARA MUJER',NULL),(115,'CAMISA PARA NIÑO',NULL),(116,'PANTALONES PARA NIÑO',NULL),(117,'VESTIDO PARA NIÑA',NULL),(118,'ROPA INTERIOR PARA NIÑOS',NULL),(119,'CAMISITAS Y VESTIDOS PARA BEBÉ',NULL),(120,'PAÑALES Y OTROS',NULL),(121,'CALZADO PARA HOMBRE',NULL),(122,'CALZADO DEPORTIVO',NULL),(123,'CALZADO PARA MUJER',NULL),(124,'CALZADO PARA NIÑOS',NULL),(125,'SERVICIO DE CONFECCIÓN',NULL),(126,'SERVICIO DE ALQUILER DE ROPA',NULL),(127,'SERVICIO DE LAVANDERÍA',NULL),(128,'SERVICIO DE REPARACIÓN DE CALZADO',NULL),(129,'SERVICIO Y ARTÍCULOS DE LIMPIEZA PARA CALZADO',NULL),(130,'CONSULTA MÉDICA GENERAL',NULL),(131,'MEDICINA ESPECIALIZADA',NULL),(132,'EXÁMENES DE LABORATORIO',NULL),(133,'IMÁGENES DIAGNÓSTICAS',NULL),(134,'SERVICIOS HOSPITALIZACIÓN Y AMBULANCIAS',NULL),(135,'MEDICINAS',NULL),(136,'OTROS GASTOS RELACIONADOS CON EL CUIDADO DE SALUD',NULL),(137,'APARATOS ORTOPÉDICOS U ORTÉSICOS',NULL),(138,'GASTO DE ASEGURAMIENTO PRIVADO',NULL),(139,'MATRÍCULAS BÁSICA Y SECUNDARIA',NULL),(140,'PENSIONES',NULL),(141,'MATRÍCULAS EDUCACIÓN SUPERIOR Y NO FORMAL',NULL),(142,'OTROS COSTOS EDUCATIVOS',NULL),(143,'TEXTOS',NULL),(144,'CUADERNOS',NULL),(145,'OTROS ARTÍCULOS ESCOLARES',NULL),(146,'OTROS GASTOS ESCOLARES',NULL),(147,'LIBROS',NULL),(148,'REVISTAS',NULL),(149,'PERIÓDICOS',NULL),(150,'OTROS ARTÍCULOS RELACIONADOS CON CULTURA',NULL),(151,'TELEVISOR',NULL),(152,'OTROS APARATOS DE VIDEO E IMAGEN',NULL),(153,'EQUIPO DE SONIDO',NULL),(154,'OTROS APARATOS DE SONIDO',NULL),(155,'SERVICIOS DE T.V.',NULL),(156,'ALQUILER DE VIDEOS Y JUEGOS ELECTRÓNICOS',NULL),(157,'SERVICIOS DE TURISMO',NULL),(158,'SERVICIOS  RELACIONADOS CON DIVERSIÓN',NULL),(159,'JUEGOS DE AZAR',NULL),(160,'REVELADO DE FOTOGRAFÍA',NULL),(161,'DISCOS',NULL),(162,'ARTÍCULOS DEPORTIVOS',NULL),(163,'VEHÍCULOS',NULL),(164,'OTROS VEHÍCULOS PARA TRANSPORTE',NULL),(165,'COMBUSTIBLE (GASOLINA)',NULL),(166,'COMPRA Y CAMBIO DE ACEITE',NULL),(167,'SERVICIO DE PARQUEADERO',NULL),(168,'SERVICIOS DE  MECÁNICA',NULL),(169,'BATERÍAS',NULL),(170,'LLANTAS',NULL),(171,'BUS URBANO',NULL),(172,'BUSETA',NULL),(173,'TAXI',NULL),(174,'OTROS MEDIOS TRANSPORTE URBANO',NULL),(175,'BUS INTERMUNICIPAL',NULL),(176,'OTROS TRANSPORTE INTERMUNICIPAL',NULL),(177,'PASAJE AÉREO',NULL),(178,'PORTE DE CARTAS',NULL),(179,'OTROS SERVICIOS',NULL),(180,'SERVICIO DE TELEFONÍA RESIDENCIAL',NULL),(181,'OTROS SERVICIOS DE TELEFONÍA',NULL),(182,'CERVEZA',NULL),(183,'AGUARDIENTE',NULL),(184,'OTRAS BEBIDAS ALCOHÓLICAS',NULL),(185,'CIGARRILLOS',NULL),(186,'ARTÍCULOS PARA LA HIGIENE ORAL',NULL),(187,'ARTÍCULOS PARA LA HIGIENE CORPORAL',NULL),(188,'ARTÍCULOS PARA LA HIGIENE Y CUIDADO FACIAL',NULL),(189,'ARTÍCULOS PARA LA CUIDADO DEL CABELLO',NULL),(190,'OTROS PRODUCTOS RELACIONADOS CUIDADO',NULL),(191,'SERVICIO DE CORTE DE CABELLO',NULL),(192,'OTROS SERVICIOS RELACIONADOS, CIUDADO PERSONAL',NULL),(193,'ARGOLLAS',NULL),(194,'RELOJES',NULL),(2000,'Promoción de Arroz',NULL),(2001,'Promosion de Platanos',NULL),(2002,'LActeos a mitad','Pague 10 y lleve 20'),(2003,'Carnes Frias','A mitad de precio solo por Hoy'),(2004,'Oferta de Arroz','null'),(2005,'Oferta de Arroz','null'),(2006,'Oferta de Arroz','null'),(2007,'Oferta de Arroz',NULL),(2008,'Maiz','null'),(3000,'Caja por 6','Una caja con 6 Productos'),(3001,'Litro','Productos Liquidos'),(3002,'Libra','Por ejemplo granos'),(3003,'Arroba','Pacas por arroba, como el Arroz'),(3004,'dsadsad','sddsd'),(3005,'dsadsad','sddsd'),(3006,'dsadsad','sddsd'),(3007,'dsadsad','sddsd'),(3008,'dsadsad','sddsd'),(3009,'dsadsad','sddsd'),(3010,'dsadsad','sddsd'),(3011,'dsadsad','sddsd'),(3012,'dsadsad','sddsd'),(3013,'dsadsad','sddsd'),(3014,'dsadsad','sddsd'),(3015,'dsadsad','sddsd'),(3016,'Oferta 20','Nuevos precios'),(3017,'dsadsad','sddsd'),(3018,'Fnd Casa','precios Bajos'),(3019,'Oferta 21','Con Porcentaje de descuento'),(3020,'dsadsad','sddsd'),(3021,'Oferta 23','Nuevos Precios'),(3022,'test','test 2'),(3023,'jkljljl','uiioui'),(3024,'Nueva','Desde '),(3025,'Oferta 31','Esta es una oferata nueva'),(3026,'Oferta ','Oferta'),(3027,'queso','parmesano');
/*!40000 ALTER TABLE `datos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `detalles_oferta`
--

LOCK TABLES `detalles_oferta` WRITE;
/*!40000 ALTER TABLE `detalles_oferta` DISABLE KEYS */;
INSERT INTO `detalles_oferta` VALUES (1,1,1,3,3000,2500),(2,1,1,4,35000,29000),(3,1,2,1,20000,17000),(5,2,2,2,4000,3700),(6,14,1,1,4545,12345),(7,14,1,1,4545,12345),(8,20,1,3,3000,2991),(9,21,2,1,20000,16000),(10,23,1,3,3000,2400),(11,24,1,4,35000,26250);
/*!40000 ALTER TABLE `detalles_oferta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `detalles_pedido`
--

LOCK TABLES `detalles_pedido` WRITE;
/*!40000 ALTER TABLE `detalles_pedido` DISABLE KEYS */;
INSERT INTO `detalles_pedido` VALUES (2,1,1,4,34,29000),(3,4,1,1,1,1),(4,5,1,1,1,1),(5,6,1,1,1,1),(6,7,1,1,1,1),(7,8,1,1,1,1),(8,9,1,1,1,1),(9,10,1,1,1,1),(10,11,1,1,1,1),(11,12,1,1,1,1),(12,13,1,1,1,1),(13,14,1,1,1,1),(14,15,1,1,1,1),(15,16,1,1,1,1),(16,17,1,1,1,1),(17,18,1,1,1,1),(18,19,1,1,1,1),(19,22,1,1,1,1),(20,23,1,3,50,2500),(21,23,1,4,12,29000),(22,23,2,1,23,17000),(23,24,2,2,56,3700),(24,25,2,2,18,3700),(25,26,1,3,36,2500),(26,26,1,4,25,29000),(27,26,2,1,40,17000),(28,27,1,3,23,2500),(29,27,2,1,52,17000),(30,28,1,3,656,2500),(31,29,2,2,32,3700),(32,30,1,3,1,2500);
/*!40000 ALTER TABLE `detalles_pedido` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `directorio`
--

LOCK TABLES `directorio` WRITE;
/*!40000 ALTER TABLE `directorio` DISABLE KEYS */;
/*!40000 ALTER TABLE `directorio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `fcm`
--

LOCK TABLES `fcm` WRITE;
/*!40000 ALTER TABLE `fcm` DISABLE KEYS */;
INSERT INTO `fcm` VALUES (1,'Hola','Desda','adsdsadsadassdasdasdasdasdasd',0,'2017-04-09 21:33:55',NULL),(2,'Hola','Desda','adsdsadsadassdasdasdasdasdasd',0,'2017-05-09 21:33:55',NULL),(3,'Hola','Desda','adsdsadsadassdasdasdasdasdasd',0,'2018-05-09 21:33:55',NULL),(4,'Oferta Oferta de Arroz','null','2:7:2',0,'2017-04-08 18:07:59',NULL),(5,'Oferta Oferta de Arroz',' Se ha creado la oferta Oferta de Arroz, puede ser de su interes','3:8:2',0,'2017-04-08 18:07:59',NULL),(6,'Oferta Promoción de Arroz',' Se ha creado la oferta Promoción de Arroz, puede ser de su interes','1:1',0,'2017-04-08 17:39:39',NULL),(7,'Oferta Maiz','null','3:8:2',0,'2017-04-08 18:07:59',NULL);
/*!40000 ALTER TABLE `fcm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `mails`
--

LOCK TABLES `mails` WRITE;
/*!40000 ALTER TABLE `mails` DISABLE KEYS */;
/*!40000 ALTER TABLE `mails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `marcas`
--

LOCK TABLES `marcas` WRITE;
/*!40000 ALTER TABLE `marcas` DISABLE KEYS */;
INSERT INTO `marcas` VALUES (1,1),(2,2),(3,3),(4,4),(5,5),(6,6),(7,7),(8,8),(9,9),(10,10);
/*!40000 ALTER TABLE `marcas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `menu_operaciones`
--

LOCK TABLES `menu_operaciones` WRITE;
/*!40000 ALTER TABLE `menu_operaciones` DISABLE KEYS */;
/*!40000 ALTER TABLE `menu_operaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `menus`
--

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` VALUES (1,1,'ofertas',123,'ofertas','icon','avatar','imagen',1),(2,2,'administrador',123,'student','des','qwer','azul',1),(3,2,'asd',123,'test','asd','qwwer','qewwe',1),(4,1,'test',123,'dddd','asd','asdf','sdfg',1),(5,1,'eeeee',1321,'qwer','qwer','qwer','asdf',1),(6,3,'Ofertas',123,'/dashboard/oferta','aaa','aaaa','aaa',1),(7,3,'Otra',123,'/dashboard/ofertas','dasda','adasd','dsdad',1),(8,3,'Lista',123,'/dashboard/oferta','asdds','asd','qewqewq',1);
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `ofertas`
--

LOCK TABLES `ofertas` WRITE;
/*!40000 ALTER TABLE `ofertas` DISABLE KEYS */;
INSERT INTO `ofertas` VALUES (1,1,NULL,2000,'2017-04-11 22:40:36','2017-06-11 22:40:48',0,4500,1),(2,1,NULL,2003,'2017-04-12 00:37:19','2017-06-13 00:37:30',1,0.3,1),(3,NULL,'http://www.example.com',3006,'1969-12-31 19:00:12','1969-12-31 19:00:01',1,122,1),(4,NULL,'http://www.example.com',3007,'1969-12-31 19:00:12','1969-12-31 19:00:01',1,122,0),(5,NULL,'http://www.example.com',3008,'1969-12-31 19:00:12','1969-12-31 19:00:01',1,122,0),(6,NULL,'http://www.example.com',3009,'1969-12-31 19:00:12','1969-12-31 19:00:01',1,122,1),(7,NULL,'http://www.example.com',3010,'1969-12-31 19:00:12','1969-12-31 19:00:01',1,122,0),(8,NULL,'http://www.example.com',3011,'1969-12-31 19:00:12','1969-12-31 19:00:01',1,122,0),(9,NULL,'http://www.example.com',3012,'1969-12-31 19:00:12','1969-12-31 19:00:01',0,122,0),(10,NULL,'http://www.example.com',3013,'1969-12-31 19:00:12','1969-12-31 19:00:01',1,NULL,1),(11,NULL,'http://www.example.com',3014,'1969-12-31 19:00:12','1969-12-31 19:00:01',1,NULL,1),(12,NULL,'http://www.example.com',3015,'2016-12-31 19:00:00','2017-12-31 19:00:00',1,NULL,1),(13,NULL,'http://www.example.com',3016,'2017-04-21 19:00:00','2017-04-28 19:00:00',1,NULL,1),(14,NULL,'http://www.example.com',3017,'1969-12-31 19:00:12','1969-12-31 19:00:01',1,NULL,1),(15,NULL,'http://www.example.com',3018,'2017-02-28 19:00:00','2017-09-27 19:00:00',1,NULL,1),(16,NULL,'http://www.example.com',3019,'2017-04-06 19:00:00','2017-05-30 19:00:00',1,NULL,1),(17,NULL,'http://www.example.com',3020,'1969-12-31 19:00:12','1969-12-31 19:00:01',1,0.3,1),(18,NULL,'http://www.example.com',3021,'2017-03-31 19:00:00','2017-05-26 19:00:00',0,200,1),(19,NULL,'http://www.cambio.com',3022,'2017-03-31 19:00:00','2017-08-25 19:00:00',1,NULL,1),(20,NULL,'http://www.example.com',3023,'1969-12-31 19:00:12','1969-12-31 19:00:01',1,0.3,1),(21,NULL,'http://www.example.com',3024,'2017-03-31 19:00:00','2017-05-30 19:00:00',1,20,1),(22,NULL,'http://www.example.com',3025,'2017-03-31 19:00:00','2017-06-29 19:00:00',1,50,1),(23,NULL,'http://www.distribuidora.com',3026,'2017-03-31 19:00:00','2017-04-29 19:00:00',1,20,1),(24,NULL,'http://www.example.com',3027,'2017-04-03 19:00:00','2017-05-25 19:00:00',1,25,1);
/*!40000 ALTER TABLE `ofertas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `operaciones`
--

LOCK TABLES `operaciones` WRITE;
/*!40000 ALTER TABLE `operaciones` DISABLE KEYS */;
/*!40000 ALTER TABLE `operaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `pedidos`
--

LOCK TABLES `pedidos` WRITE;
/*!40000 ALTER TABLE `pedidos` DISABLE KEYS */;
INSERT INTO `pedidos` VALUES (1,1,3456780,1,0,'2017-04-12 03:43:10'),(2,1,1222,1,1,NULL),(3,1,2332,1,0,NULL),(4,1,2332,1,0,NULL),(5,1,2332,1,0,NULL),(6,1,2332,1,0,NULL),(7,1,2332,1,0,NULL),(8,1,2332,1,0,NULL),(9,1,2332,1,0,NULL),(10,1,2332,1,0,NULL),(11,1,2332,1,0,NULL),(12,1,2332,1,0,NULL),(13,1,2332,1,0,NULL),(14,1,2332,1,0,NULL),(15,1,2332,1,0,NULL),(16,1,2332,1,0,NULL),(17,1,2332,1,0,NULL),(18,1,2332,1,0,NULL),(19,1,2332,1,0,NULL),(22,1,2332,1,0,NULL),(23,1,2332,1,0,NULL),(24,1,2332,2,0,'2017-04-18 15:04:00'),(25,1,2332,2,0,'2017-04-18 15:04:37'),(26,1,2332,1,0,'2017-04-18 15:04:56'),(27,1,2332,1,0,'2017-04-20 15:18:26'),(28,1,2332,1,0,'2017-04-20 15:18:53'),(29,1,2332,2,0,'2017-04-23 18:31:51'),(30,1,2332,1,0,'2017-04-27 12:59:20');
/*!40000 ALTER TABLE `pedidos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `permisos_api`
--

LOCK TABLES `permisos_api` WRITE;
/*!40000 ALTER TABLE `permisos_api` DISABLE KEYS */;
/*!40000 ALTER TABLE `permisos_api` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `persona`
--

LOCK TABLES `persona` WRITE;
/*!40000 ALTER TABLE `persona` DISABLE KEYS */;
/*!40000 ALTER TABLE `persona` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `personas`
--

LOCK TABLES `personas` WRITE;
/*!40000 ALTER TABLE `personas` DISABLE KEYS */;
INSERT INTO `personas` VALUES (1,5,NULL,NULL,'Proveedor','Gabriel','Marquez','Urtadp'),(2,3,NULL,NULL,'Luis','Fernando','Garcia','Quiroga'),(4,4,NULL,NULL,'Luis','Fernando','Garcia','Quiroga');
/*!40000 ALTER TABLE `personas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `productos`
--

LOCK TABLES `productos` WRITE;
/*!40000 ALTER TABLE `productos` DISABLE KEYS */;
INSERT INTO `productos` VALUES (1,22,11,1,NULL),(2,23,11,2,NULL),(3,24,11,1,NULL),(4,25,11,1,NULL),(5,26,11,1,NULL),(6,27,11,1,NULL),(7,28,11,1,NULL),(8,29,11,2,NULL),(9,30,11,1,NULL),(10,31,11,1,NULL),(11,32,11,1,NULL),(12,33,11,1,NULL),(13,34,11,1,NULL),(14,35,11,1,NULL),(15,36,11,1,NULL),(16,37,11,1,NULL),(17,38,11,1,NULL),(18,39,11,1,NULL),(19,40,11,1,NULL),(20,41,11,1,NULL),(21,42,11,1,NULL),(22,43,11,1,NULL),(23,45,11,4,NULL),(24,46,11,1,NULL),(25,47,11,1,NULL),(26,48,11,1,NULL),(27,49,11,1,NULL),(28,50,11,6,NULL),(29,51,11,1,NULL),(30,52,11,1,NULL),(31,53,11,1,NULL),(32,54,11,1,NULL),(33,55,11,4,NULL),(34,56,11,1,NULL);
/*!40000 ALTER TABLE `productos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `proveedor`
--

LOCK TABLES `proveedor` WRITE;
/*!40000 ALTER TABLE `proveedor` DISABLE KEYS */;
INSERT INTO `proveedor` VALUES (1,1,250);
/*!40000 ALTER TABLE `proveedor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `repositorio`
--

LOCK TABLES `repositorio` WRITE;
/*!40000 ALTER TABLE `repositorio` DISABLE KEYS */;
/*!40000 ALTER TABLE `repositorio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,1,'TIENDA','DE TIENDAS'),(2,2,'admin','administrador'),(3,2,'tes','rol test'),(4,2,'otror','otro rol'),(5,1,'info','informativo'),(6,1,'rol','roless'),(7,3,'proveedor','Proveedor, Registra las Ofertas'),(8,3,'test','test');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `roles_menu`
--

LOCK TABLES `roles_menu` WRITE;
/*!40000 ALTER TABLE `roles_menu` DISABLE KEYS */;
INSERT INTO `roles_menu` VALUES (1,1,1),(4,1,4),(5,1,5),(2,2,2),(3,4,3),(8,7,6),(11,7,8),(13,8,6),(12,8,7),(10,8,8);
/*!40000 ALTER TABLE `roles_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `roles_permisos_api`
--

LOCK TABLES `roles_permisos_api` WRITE;
/*!40000 ALTER TABLE `roles_permisos_api` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles_permisos_api` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `sesiones`
--

LOCK TABLES `sesiones` WRITE;
/*!40000 ALTER TABLE `sesiones` DISABLE KEYS */;
/*!40000 ALTER TABLE `sesiones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `telefonos`
--

LOCK TABLES `telefonos` WRITE;
/*!40000 ALTER TABLE `telefonos` DISABLE KEYS */;
/*!40000 ALTER TABLE `telefonos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tokens`
--

LOCK TABLES `tokens` WRITE;
/*!40000 ALTER TABLE `tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `unidades`
--

LOCK TABLES `unidades` WRITE;
/*!40000 ALTER TABLE `unidades` DISABLE KEYS */;
INSERT INTO `unidades` VALUES (1,3000),(2,3001),(3,3002),(4,3003);
/*!40000 ALTER TABLE `unidades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `usuario_app`
--

LOCK TABLES `usuario_app` WRITE;
/*!40000 ALTER TABLE `usuario_app` DISABLE KEYS */;
INSERT INTO `usuario_app` VALUES (1,'luis','qwe'),(2,'administrador','12345678'),(3,'authofertas','qwe'),(4,'admin','admin');
/*!40000 ALTER TABLE `usuario_app` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,1,'user','user'),(2,2,'monitor','qwe'),(3,1,'wilson','qwe'),(4,3,'proveedor','qwe'),(5,3,'proveedor2','qwe123'),(6,3,'otro','qwe'),(7,3,'luis','luis'),(8,3,'juan','juan');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `usuarios_roles`
--

LOCK TABLES `usuarios_roles` WRITE;
/*!40000 ALTER TABLE `usuarios_roles` DISABLE KEYS */;
INSERT INTO `usuarios_roles` VALUES (1,1,1),(5,2,2),(3,3,2),(4,4,2),(6,1,3),(7,5,3),(8,7,4),(9,7,5),(10,7,6),(11,8,6),(12,8,7),(14,7,8),(15,8,8);
/*!40000 ALTER TABLE `usuarios_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `valor_unidad`
--

LOCK TABLES `valor_unidad` WRITE;
/*!40000 ALTER TABLE `valor_unidad` DISABLE KEYS */;
INSERT INTO `valor_unidad` VALUES (1,3,1,3000),(2,4,1,35000),(3,2,2,4000),(4,1,2,20000);
/*!40000 ALTER TABLE `valor_unidad` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-27 20:57:41
