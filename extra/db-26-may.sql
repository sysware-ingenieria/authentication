CREATE DATABASE  IF NOT EXISTS `auth` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `auth`;
-- MySQL dump 10.13  Distrib 5.7.17, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: auth
-- ------------------------------------------------------
-- Server version	5.7.18-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `aplicaciones`
--

DROP TABLE IF EXISTS `aplicaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aplicaciones` (
  `id_aplicacion` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario_app` int(11) NOT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `descripcion` varchar(200) DEFAULT NULL,
  `version` varchar(50) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT CURRENT_TIMESTAMP,
  `fecha_actualizacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `key_aplicacion` text,
  `key_servidor` text,
  PRIMARY KEY (`id_aplicacion`),
  UNIQUE KEY `aplicaciones_id_aplicacion_uindex` (`id_aplicacion`),
  UNIQUE KEY `aplicaciones_nombre_id_usuario_app_uindex` (`nombre`,`id_usuario_app`),
  KEY `aplicaciones_usuario_app_id_user_app_fk` (`id_usuario_app`),
  CONSTRAINT `aplicaciones_usuario_app_id_user_app_fk` FOREIGN KEY (`id_usuario_app`) REFERENCES `usuario_app` (`id_usuario_app`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aplicaciones`
--

LOCK TABLES `aplicaciones` WRITE;
/*!40000 ALTER TABLE `aplicaciones` DISABLE KEYS */;
INSERT INTO `aplicaciones` VALUES (1,1,'Ofertas','Tiendas y surtidos','v1','2017-05-16 10:17:51','2017-05-16 15:17:51','BZzaSyDDvpCczVBNx_YFb7mpxLOON2g6XKtO4Cw','dt5fR_Mtm9'),(2,2,'AppRuta','Administracion rutas de colegios','r1.0','2017-05-16 11:07:06','2017-05-16 16:07:06','BZzaSyDDvpCczVBNx_YFb7mpxLOON2g6XKtO4Cw','dt5fR_Mtm9');
/*!40000 ALTER TABLE `aplicaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `directorio`
--

DROP TABLE IF EXISTS `directorio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `directorio` (
  `id_directorio` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_directorio`),
  UNIQUE KEY `directorio_id_directorio_uindex` (`id_directorio`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `directorio`
--

LOCK TABLES `directorio` WRITE;
/*!40000 ALTER TABLE `directorio` DISABLE KEYS */;
/*!40000 ALTER TABLE `directorio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mails`
--

DROP TABLE IF EXISTS `mails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mails` (
  `id_mail` int(11) NOT NULL AUTO_INCREMENT,
  `mail` varchar(200) DEFAULT NULL,
  `tipo` varchar(50) DEFAULT NULL,
  `preferencia` int(11) DEFAULT NULL,
  `id_directorio` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_mail`),
  UNIQUE KEY `mails_id_mail_uindex` (`id_mail`),
  KEY `mails_directorio_id_directorio_fk` (`id_directorio`),
  CONSTRAINT `mails_directorio_id_directorio_fk` FOREIGN KEY (`id_directorio`) REFERENCES `directorio` (`id_directorio`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mails`
--

LOCK TABLES `mails` WRITE;
/*!40000 ALTER TABLE `mails` DISABLE KEYS */;
/*!40000 ALTER TABLE `mails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_operaciones`
--

DROP TABLE IF EXISTS `menu_operaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu_operaciones` (
  `id_menu_operacion` int(11) NOT NULL AUTO_INCREMENT,
  `id_menu` int(11) DEFAULT NULL,
  `id_operacion` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_menu_operacion`),
  KEY `menu_operaciones_menus_id_menu_fk` (`id_menu`),
  KEY `menu_operaciones_operaciones_id_operacion_fk` (`id_operacion`),
  CONSTRAINT `menu_operaciones_menus_id_menu_fk` FOREIGN KEY (`id_menu`) REFERENCES `menus` (`id_menu`),
  CONSTRAINT `menu_operaciones_operaciones_id_operacion_fk` FOREIGN KEY (`id_operacion`) REFERENCES `operaciones` (`id_operacion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_operaciones`
--

LOCK TABLES `menu_operaciones` WRITE;
/*!40000 ALTER TABLE `menu_operaciones` DISABLE KEYS */;
/*!40000 ALTER TABLE `menu_operaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menus` (
  `id_menu` int(11) NOT NULL AUTO_INCREMENT,
  `id_aplicacion` int(11) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `tipo` int(11) NOT NULL,
  `ruta` text,
  `icono` text,
  `avatar` text,
  `background` text,
  `estado` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_menu`),
  KEY `menus_aplicaciones_id_aplicacion_fk` (`id_aplicacion`),
  CONSTRAINT `menus_aplicaciones_id_aplicacion_fk` FOREIGN KEY (`id_aplicacion`) REFERENCES `aplicaciones` (`id_aplicacion`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus`
--

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` VALUES (1,1,'ofertas',123,'ofertas','icon','avatar','imagen',1),(2,2,'administrador',123,'student','des','qwer','azul',1),(3,2,'asd',123,'test','asd','qwwer','qewwe',1),(4,1,'test',123,'dddd','asd','asdf','sdfg',1),(5,1,'eeeee',1321,'qwer','qwer','qwer','asdf',1);
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `operaciones`
--

DROP TABLE IF EXISTS `operaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `operaciones` (
  `id_operacion` int(11) NOT NULL AUTO_INCREMENT,
  `id_aplicacion` int(11) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_operacion`),
  UNIQUE KEY `operaciones_id_operacion_uindex` (`id_operacion`),
  KEY `operaciones_aplicaciones_id_aplicacion_fk` (`id_aplicacion`),
  CONSTRAINT `operaciones_aplicaciones_id_aplicacion_fk` FOREIGN KEY (`id_aplicacion`) REFERENCES `aplicaciones` (`id_aplicacion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `operaciones`
--

LOCK TABLES `operaciones` WRITE;
/*!40000 ALTER TABLE `operaciones` DISABLE KEYS */;
/*!40000 ALTER TABLE `operaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permisos_api`
--

DROP TABLE IF EXISTS `permisos_api`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permisos_api` (
  `id_permiso_api` int(11) NOT NULL AUTO_INCREMENT,
  `id_aplicacion` int(11) NOT NULL,
  `id_api` int(11) DEFAULT NULL,
  `http_verb` varchar(20) NOT NULL,
  `http_uri` varchar(500) NOT NULL,
  PRIMARY KEY (`id_permiso_api`),
  UNIQUE KEY `permisos_api_id_permiso_api_uindex` (`id_permiso_api`),
  UNIQUE KEY `permisos_api_id_aplicacion_http_uri_http_verb_uindex` (`id_aplicacion`,`http_uri`,`http_verb`),
  CONSTRAINT `permisos_api_aplicaciones_id_aplicacion_fk` FOREIGN KEY (`id_aplicacion`) REFERENCES `aplicaciones` (`id_aplicacion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permisos_api`
--

LOCK TABLES `permisos_api` WRITE;
/*!40000 ALTER TABLE `permisos_api` DISABLE KEYS */;
/*!40000 ALTER TABLE `permisos_api` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persona`
--

DROP TABLE IF EXISTS `persona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persona` (
  `id_person` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) DEFAULT NULL,
  `id_directorio` int(11) DEFAULT NULL,
  `primer_nombre` varchar(50) NOT NULL,
  `segundo_nombre` varchar(50) DEFAULT NULL,
  `primer_apellido` varchar(50) NOT NULL,
  `segundo_apellido` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_person`),
  UNIQUE KEY `persona_id_person_uindex` (`id_person`),
  KEY `persona_usuarios_id_usuario_fk` (`id_usuario`),
  KEY `persona_directorio_id_directorio_fk` (`id_directorio`),
  CONSTRAINT `persona_directorio_id_directorio_fk` FOREIGN KEY (`id_directorio`) REFERENCES `directorio` (`id_directorio`),
  CONSTRAINT `persona_usuarios_id_usuario_fk` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persona`
--

LOCK TABLES `persona` WRITE;
/*!40000 ALTER TABLE `persona` DISABLE KEYS */;
/*!40000 ALTER TABLE `persona` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `repositorio`
--

DROP TABLE IF EXISTS `repositorio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repositorio` (
  `id_bitacora` int(11) NOT NULL AUTO_INCREMENT,
  `id_aplicacion` int(11) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `operacion` varchar(100) DEFAULT NULL,
  `tipo` varchar(100) DEFAULT 'TABLA',
  `descricion` varchar(100) DEFAULT NULL,
  `fecha_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fecha_actualizacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_bitacora`),
  UNIQUE KEY `bitacora_id_bitacora_uindex` (`id_bitacora`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `repositorio`
--

LOCK TABLES `repositorio` WRITE;
/*!40000 ALTER TABLE `repositorio` DISABLE KEYS */;
/*!40000 ALTER TABLE `repositorio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id_rol` int(11) NOT NULL AUTO_INCREMENT,
  `id_aplicacion` int(11) NOT NULL,
  `rol` varchar(100) NOT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_rol`),
  UNIQUE KEY `roles_id_aplicacion_rol_uindex` (`id_aplicacion`,`rol`),
  CONSTRAINT `roles_aplicaciones_id_aplicacion_fk` FOREIGN KEY (`id_aplicacion`) REFERENCES `aplicaciones` (`id_aplicacion`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,1,'TIENDA','DE TIENDAS'),(2,2,'admin','administrador'),(3,2,'tes','rol test'),(4,2,'otror','otro rol'),(5,1,'info','informativo'),(6,1,'rol','roless');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles_menu`
--

DROP TABLE IF EXISTS `roles_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles_menu` (
  `id_rol_menu` int(11) NOT NULL AUTO_INCREMENT,
  `id_rol` int(11) DEFAULT NULL,
  `id_menu` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_rol_menu`),
  UNIQUE KEY `roles_menu_id_rol_menu_uindex` (`id_rol_menu`),
  UNIQUE KEY `roles_menu_id_rol_id_menu_uindex` (`id_rol`,`id_menu`),
  KEY `roles_menu_menus_id_menu_fk` (`id_menu`),
  CONSTRAINT `roles_menu_menus_id_menu_fk` FOREIGN KEY (`id_menu`) REFERENCES `menus` (`id_menu`),
  CONSTRAINT `roles_menu_roles_id_rol_fk` FOREIGN KEY (`id_rol`) REFERENCES `roles` (`id_rol`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles_menu`
--

LOCK TABLES `roles_menu` WRITE;
/*!40000 ALTER TABLE `roles_menu` DISABLE KEYS */;
INSERT INTO `roles_menu` VALUES (1,1,1),(2,2,2),(3,4,3);
/*!40000 ALTER TABLE `roles_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles_permisos_api`
--

DROP TABLE IF EXISTS `roles_permisos_api`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles_permisos_api` (
  `id_rol_permiso` int(11) NOT NULL AUTO_INCREMENT,
  `id_rol` int(11) NOT NULL,
  `id_permiso_api` int(11) NOT NULL,
  PRIMARY KEY (`id_rol_permiso`),
  UNIQUE KEY `roles_permisos_api_id_rol_permiso_uindex` (`id_rol_permiso`),
  KEY `roles_permisos_api_roles_id_rol_fk` (`id_rol`),
  KEY `roles_permisos_api_permisos_api_id_permiso_api_fk` (`id_permiso_api`),
  CONSTRAINT `roles_permisos_api_permisos_api_id_permiso_api_fk` FOREIGN KEY (`id_permiso_api`) REFERENCES `permisos_api` (`id_permiso_api`),
  CONSTRAINT `roles_permisos_api_roles_id_rol_fk` FOREIGN KEY (`id_rol`) REFERENCES `roles` (`id_rol`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles_permisos_api`
--

LOCK TABLES `roles_permisos_api` WRITE;
/*!40000 ALTER TABLE `roles_permisos_api` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles_permisos_api` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sesiones`
--

DROP TABLE IF EXISTS `sesiones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sesiones` (
  `id_sesion` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) DEFAULT NULL,
  `id_token` int(11) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT CURRENT_TIMESTAMP,
  `tipo` varchar(50) DEFAULT NULL,
  `dispositivo` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_sesion`),
  UNIQUE KEY `sesiones_id_sesion_uindex` (`id_sesion`),
  KEY `sesiones_usuarios_id_usuario_fk` (`id_usuario`),
  KEY `sesiones_tokens_id_token_fk` (`id_token`),
  CONSTRAINT `sesiones_tokens_id_token_fk` FOREIGN KEY (`id_token`) REFERENCES `tokens` (`id_token`),
  CONSTRAINT `sesiones_usuarios_id_usuario_fk` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sesiones`
--

LOCK TABLES `sesiones` WRITE;
/*!40000 ALTER TABLE `sesiones` DISABLE KEYS */;
/*!40000 ALTER TABLE `sesiones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `telefonos`
--

DROP TABLE IF EXISTS `telefonos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `telefonos` (
  `id_telefono` int(11) NOT NULL AUTO_INCREMENT,
  `id_directorio` int(11) NOT NULL,
  `numero` varchar(50) DEFAULT NULL,
  `tipo` varchar(50) DEFAULT NULL,
  `preferencia` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_telefono`),
  UNIQUE KEY `telefonos_id_telefono_uindex` (`id_telefono`),
  KEY `telefonos_directorio_id_directorio_fk` (`id_directorio`),
  CONSTRAINT `telefonos_directorio_id_directorio_fk` FOREIGN KEY (`id_directorio`) REFERENCES `directorio` (`id_directorio`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `telefonos`
--

LOCK TABLES `telefonos` WRITE;
/*!40000 ALTER TABLE `telefonos` DISABLE KEYS */;
/*!40000 ALTER TABLE `telefonos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tokens`
--

DROP TABLE IF EXISTS `tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tokens` (
  `id_token` int(11) NOT NULL AUTO_INCREMENT,
  `key_token` text NOT NULL,
  `duracion` datetime DEFAULT NULL,
  `fecha_creacion` datetime NOT NULL,
  `fecha_actualizacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `key_servidor` text,
  PRIMARY KEY (`id_token`),
  UNIQUE KEY `tokens_id_token_uindex` (`id_token`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tokens`
--

LOCK TABLES `tokens` WRITE;
/*!40000 ALTER TABLE `tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario_app`
--

DROP TABLE IF EXISTS `usuario_app`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario_app` (
  `id_usuario_app` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(50) NOT NULL,
  `clave` varchar(600) NOT NULL,
  PRIMARY KEY (`id_usuario_app`),
  UNIQUE KEY `usuario_app_username_uindex` (`usuario`),
  UNIQUE KEY `usuario_app_id_aplicacion_usuario_uindex` (`usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario_app`
--

LOCK TABLES `usuario_app` WRITE;
/*!40000 ALTER TABLE `usuario_app` DISABLE KEYS */;
INSERT INTO `usuario_app` VALUES (1,'luis','qwe'),(2,'administrador','12345678');
/*!40000 ALTER TABLE `usuario_app` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `id_aplicacion` int(11) DEFAULT NULL,
  `usuario` varchar(50) NOT NULL,
  `clave` varchar(600) NOT NULL,
  PRIMARY KEY (`id_usuario`),
  UNIQUE KEY `usuarios_id_aplicacion_usuario_uindex` (`id_aplicacion`,`usuario`),
  CONSTRAINT `usuarios_aplicaciones_id_aplicacion_fk` FOREIGN KEY (`id_aplicacion`) REFERENCES `aplicaciones` (`id_aplicacion`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,1,'user','user'),(2,2,'monitor','qwe'),(3,1,'wilson','qwe');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios_roles`
--

DROP TABLE IF EXISTS `usuarios_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios_roles` (
  `id_usuario_rol` int(11) NOT NULL AUTO_INCREMENT,
  `id_rol` int(11) DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_usuario_rol`),
  UNIQUE KEY `usuarios_roles_id_usuario_id_rol_uindex` (`id_usuario`,`id_rol`),
  KEY `usuarios_roles_roles_id_rol_fk` (`id_rol`),
  CONSTRAINT `usuarios_roles_roles_id_rol_fk` FOREIGN KEY (`id_rol`) REFERENCES `roles` (`id_rol`),
  CONSTRAINT `usuarios_roles_usuarios_id_usuario_fk` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios_roles`
--

LOCK TABLES `usuarios_roles` WRITE;
/*!40000 ALTER TABLE `usuarios_roles` DISABLE KEYS */;
INSERT INTO `usuarios_roles` VALUES (1,1,1),(5,2,2),(3,3,2),(4,4,2),(6,1,3);
/*!40000 ALTER TABLE `usuarios_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'auth'
--

--
-- Dumping routines for database 'auth'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-16 15:00:25
