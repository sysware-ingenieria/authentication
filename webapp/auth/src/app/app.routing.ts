import { Routes } from '@angular/router';

/***************************************************
 * Llamar el Componente que se muestra por defecto *
 ***************************************************/
import { IndexComponent } from './welcome/index/index.component'

/***************************************************************
 * Llamar a todas las Rutas que tenga en cada Modulo de la App  *
 ***************************************************************/
import { WelcomeRouting }  from './welcome/welcome-routing'
import { AuthenticationRouting }  from './authentication/authentication.routing'
import { DashboardRouting } from './client/dashboard/index';
import { ProfileRouting } from './client/profile/profile.routing'


import { AuthGuard } from './shared/guard/auth.guard';

export const AppRouting: Routes = [
    { path: 'not-found', loadChildren: './not-found/not-found.module#NotFoundModule' },
  ...WelcomeRouting,
  ...AuthenticationRouting,
  ...DashboardRouting,
  ...ProfileRouting,
  
  { path: '**', redirectTo: 'not-found' }
];
