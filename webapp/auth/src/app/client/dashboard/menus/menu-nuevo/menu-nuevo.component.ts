import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params }   from '@angular/router';
import { FormGroup, FormBuilder , Validators } from '@angular/forms';
import { Location } from '@angular/common';
import {MdDialog, MdDialogRef} from '@angular/material';


/********************************
 *          Modelos 
 ********************************/

/********************************
 *          Servicios 
 ********************************/
@Component({
  moduleId:module.id,
  selector: 'app-menu-nuevo',
  templateUrl: './menu-nuevo.component.html',
  styleUrls: ['./menu-nuevo.component.scss']
})
export class MenuNuevoComponent implements OnInit {

   action = 'Crear';
   title = 'Registrar una Nueva Aplicación';
   form: FormGroup;

   

  constructor(public dialogRef: MdDialogRef<MenuNuevoComponent>,
  public fb: FormBuilder,) {
     this.controlsCreate();
  }


  ngOnInit() {
  }

          // start controls from from
  public controlsCreate() {
    this.form = this.fb.group({
       nombre: ['', Validators.compose([
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(50),
            Validators.pattern('[a-zA-Z]{1}[a-z A-Z0-9-]{0,48}')
          ])],
      
      tipo: ['', Validators.compose([
             Validators.required,
            Validators.minLength(3),
            Validators.maxLength(100),
            
          ])],
    ruta: ['', Validators.compose([
             Validators.required,
            Validators.minLength(1),
            Validators.maxLength(50)
          ])],
     icono: ['', Validators.compose([
             Validators.required,
            Validators.minLength(1),
            Validators.maxLength(50),
            Validators.pattern('[a-zA-Z]{1}[a-z A-Z0-9-(,).]{0,9}')
          ])],
     avatar: ['', Validators.compose([
             Validators.required,
            Validators.minLength(1),
            Validators.maxLength(50),
            Validators.pattern('[a-zA-Z]{1}[a-z A-Z0-9-(,).]{0,9}')
          ])],
    background: ['', Validators.compose([
             Validators.required,
            Validators.minLength(1),
            Validators.maxLength(50),
            Validators.pattern('[a-zA-Z]{1}[a-z A-Z0-9-(,).]{0,9}')
          ])],
  estado: ['', Validators.compose([
             Validators.required,
            Validators.minLength(1),
            Validators.maxLength(1),
            Validators.pattern('[0-1]{1}')
          ])],
    }); 

  }

}
