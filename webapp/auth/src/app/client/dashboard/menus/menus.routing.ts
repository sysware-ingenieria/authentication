import { Routes } from '@angular/router';

/********************************
 *          Componentes 
 ********************************/
import { MenuListaComponent } from './menu-lista/menu-lista.component';
import { MenuDetalleComponent } from './menu-detalle/menu-detalle.component';
import { MenuNuevoComponent } from './menu-nuevo/menu-nuevo.component';

export const MenusRouting: Routes = [
    {
      path: 'menu',
      component: null,
      children: [
        {path: '', redirectTo: 'lista', pathMatch: 'full'},
        {path: 'lista', component: MenuListaComponent},
        { path: 'detalle/:id',     component: MenuDetalleComponent },
        { path: 'nuevo',     component: MenuNuevoComponent},
      ]
    }
];

