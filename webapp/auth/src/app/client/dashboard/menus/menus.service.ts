import { Injectable } from '@angular/core';
import { Http, Headers, Response, URLSearchParams, RequestOptions } from '@angular/http';

import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs/Observable'


import {urlbase} from '../../../shared/url';
import { LocalStorage } from '../../../shared/localStorage';
import { Token } from '../../../shared/token';

// Data model
import { Menu } from './menu'
import { MenuDTO } from './menuDTO'

import { ResponseAPI } from '../../../shared/utils/ResponseAPI';


@Injectable()
export class MenusService {

  private api_uri = urlbase[0] + '/menus';


    private headers = new Headers({'Content-Type': 'application/json'});
    private options: RequestOptions;
    private token: Token;

      constructor(private http: Http, private locStorage: LocalStorage) {
        let aux =  this.locStorage.getData();
        /*}let headers = new Headers({
          'Content-Type': 'application/json',
          'Authorization' : this.locStorage.getTokenValue()
        });*/
        if(aux===null){

        }else{
            
        }

            this.token= new Token(null,null,null);
            this.token.id_usuario_app=aux.id_usuario_app;
            this.token.usuario=aux.usuario;
            this.options = new RequestOptions(this.headers);
        
      }


        public getMenus= (id_menu:number|null,id_aplicacion:number|null,  nombre:string|null,tipo:number|null,
            ruta:string|null,icono:string|null,avatar:string|null,background:string|null,estato:number|null): Observable<Menu[]> => {

            let params: URLSearchParams = new URLSearchParams();
            params.set('id_menu',id_menu==null?null:id_menu+'');
            params.set('id_aplicacion',id_aplicacion==null?null:id_aplicacion+'');
            params.set('nombre',  nombre==null?null:nombre+'');
            params.set('tipo',tipo==null?null:tipo+'');
            params.set('ruta', ruta==null?null:ruta+'');
            params.set('icono', icono==null?null:icono+'');
            params.set('avatar', avatar==null?null:avatar+'');
            params.set('background', background==null?null:background+'');
            params.set('estato', estato==null?null:estato+'');
            
            
            let myOption: RequestOptions = this.options;
            myOption.search = params;
            

        return this.http.get(this.api_uri, myOption)
            .map((response: Response) => <Menu[]>response.json())
            .catch(this.handleError);
    }

      

      public postMenu = (menuDTO: MenuDTO): Observable<Response> => {
        let toAdd = JSON.stringify({menuDTO});
         let url=`${this.api_uri}?id_usuario_app=${this.token.id_usuario_app}`
        return this.http.post(url, menuDTO, this.options)
            .map((response: Response) => {
              
                  
                  return response;
                })
            .catch(this.handleError);

    }

    private handleError (error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
        const body = error.json() || '';
        const err = body.error || JSON.stringify(body);
        errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
        errMsg = error.message ? error.message : error.toString();
        }
        alert(errMsg);
        return Observable.throw(errMsg);
    }
}
