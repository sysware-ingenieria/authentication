import { Routes } from '@angular/router';

/********************************
 *       Componente Principal
 ********************************/
import { DashboardComponent } from './index';




/********************************
 *     Rutas de cada Modulo
 ********************************/
import { HomeRouting } from './home/index';
import { MenusRouting } from './menus/index';
import { RolesRouting } from './roles/index';
import { UsuariosRouting } from './usuarios/index';


/********************************
 *          Modelos 
 ********************************/

/********************************
 *          Servicios 
 ********************************/




export const DashboardRouting: Routes = [
  {
    path: 'dashboard' ,
    component: DashboardComponent,
    children: [
      { path: '', redirectTo: 'home', pathMatch: 'full'},
      ...HomeRouting,
      ...MenusRouting,
      ...RolesRouting,
      ...UsuariosRouting
    ]

  }
];
