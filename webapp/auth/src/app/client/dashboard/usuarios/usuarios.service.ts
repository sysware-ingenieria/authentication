import { Injectable } from '@angular/core';
import { Http, Headers, Response, URLSearchParams, RequestOptions } from '@angular/http';

import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs/Observable'

// Constante
import {urlbase} from '../../../shared/url';
import { LocalStorage } from '../../../shared/localStorage';
import { Token } from '../../../shared/token';

// Data model
import { Usuario } from './usuario'
import { UsuarioDTO } from './usuarioDTO'

import { ResponseAPI } from '../../../shared/utils/ResponseAPI';

@Injectable()
export class UsuariosService {
   private api_uri = urlbase[0] + '/usuarios';
    private headers = new Headers({'Content-Type': 'application/json'});
    private options: RequestOptions;
    private token: Token;

   constructor(private http: Http, private locStorage: LocalStorage) {
        let aux =  this.locStorage.getData();
        /*}let headers = new Headers({
          'Content-Type': 'application/json',
          'Authorization' : this.locStorage.getTokenValue()
        });*/
       
            this.token= new Token(null,null,null);
            this.token.id_usuario_app=aux.id_usuario_app;
            this.token.usuario=aux.usuario;
            this.options = new RequestOptions(this.headers);
        
      }


      public getUsuarios= (id_aplicacion:number|null,  id_usuario:number|null, usuario:string|null, clave:string|null): Observable<Usuario[]> => {

            let params: URLSearchParams = new URLSearchParams();
            params.set('id_aplicacion',id_aplicacion==null?null:id_aplicacion+'');
            params.set('id_usuario',  id_usuario==null?null:id_usuario+'');
            params.set('usuario',usuario==null?null:usuario+'');
            params.set('clave', clave==null?null:clave+'');
            
            
            let myOption: RequestOptions = this.options;
            myOption.search = params;
            

        return this.http.get(this.api_uri, myOption)
            .map((response: Response) => <Usuario[]>response.json())
            .catch(this.handleError);
    }


     public postUsuario = (usuarioDTO: UsuarioDTO): Observable<Response> => {
        let toAdd = JSON.stringify({usuarioDTO});
         let url=`${this.api_uri}`
        return this.http.post(url, usuarioDTO, this.options)
            .map((response: Response) => {
              
                  
                  return response;
                })
            .catch(this.handleError);

    }


       public getIdRolesPorUsuario= (id_rol:number|null,  id_usuario:number|null): Observable<number[]> => {

            let params: URLSearchParams = new URLSearchParams();
            
            params.set('id_rol',  id_rol==null?null:id_rol+'');
            params.set('id_usuario',id_usuario==null?null:id_usuario+'');
            let myOption: RequestOptions = this.options;
            myOption.search = params;
            

        return this.http.get(`${this.api_uri}/rol`, myOption)
            .map((response: Response) => <number[]>response.json())
            .catch(this.handleError);
    }

    public deleteRolesUsuario = (id_usuario:number, id_RolUsuarioLista:number[]): Observable<Response> => {
         
            let params: URLSearchParams = new URLSearchParams();
            
            params.set('id_usuario',id_usuario==null?null:id_usuario+'');
            
            
            let myOption: RequestOptions = this.options;
            myOption.search = params;
            
        return this.http.put(this.api_uri+"/rol", id_RolUsuarioLista, myOption)
            .map((response: Response) => {
              
                  
                  return response;
                })
            .catch(this.handleError);

    }

      public putUsuario = (id_usuario:number, usuarioDTO:UsuarioDTO): Observable<Response> => {
    
          let params: URLSearchParams = new URLSearchParams();
            params.set('id_usuario',id_usuario==null?null:id_usuario+'');
            let myOption: RequestOptions = this.options;
            myOption.search = params;

        return this.http.put(this.api_uri, usuarioDTO, myOption)
            .map((response: Response) => {
                  return response;
                })
            .catch(this.handleError);

    }


     private handleError (error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
        const body = error.json() || '';
        const err = body.error || JSON.stringify(body);
        errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
        errMsg = error.message ? error.message : error.toString();
        }
        alert(errMsg);
        return Observable.throw(errMsg);
    }




}
