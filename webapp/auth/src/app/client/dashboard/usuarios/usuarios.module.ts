import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';

/********************************
 *          Componentes 
 ********************************/
import { UsuarioNuevoComponent } from './usuario-nuevo/usuario-nuevo.component';
import { UsuarioDetalleComponent } from './usuario-detalle/usuario-detalle.component';
import { UsuarioListaComponent } from './usuario-lista/usuario-lista.component';

/********************************
 *          Modelos 
 ********************************/

/********************************
 *          Servicios 
 ********************************/
import { UsuariosService } from './usuarios.service';
import { RolesService } from '../roles/roles.service';




@NgModule({
  imports: [
      CommonModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
  ],
  declarations: [UsuarioNuevoComponent, UsuarioDetalleComponent, UsuarioListaComponent],
  exports: [UsuarioListaComponent],
   providers: [UsuariosService,RolesService]
})
export class UsuariosModule { }
