import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params }   from '@angular/router';
import { FormGroup, FormBuilder, Validators }   from '@angular/forms';
import { Location } from '@angular/common';
import { MdDialog, MdDialogRef } from '@angular/material';

/********************************
 *          Constantes 
 ********************************/
import { LocalStorage }  from '../../../../shared/localStorage';
/********************************
 *          Componentes 
 ********************************/
import { UsuarioNuevoComponent }  from '../usuario-nuevo/usuario-nuevo.component';


/********************************
 *          Modelos 
 ********************************/
import { Usuario } from '../usuario';
import { UsuarioDTO } from '../usuarioDTO';
/********************************
 *          Servicios 
 ********************************/
import { UsuariosService } from '../usuarios.service';
import { RolesService } from '../../roles/roles.service';

@Component({
  selector: 'app-usuario-lista',
  templateUrl: './usuario-lista.component.html',
  styleUrls: ['./usuario-lista.component.scss']
})
export class UsuarioListaComponent implements OnInit {

    usuarios:Usuario[];
    form: FormGroup;
    usuarioDTO:UsuarioDTO;
    id_aplicacion=null;

    constructor(public fb: FormBuilder,
              public locStorage: LocalStorage,
              public dialog: MdDialog,
              private route: ActivatedRoute,
              public router: Router,
              public location: Location,
              private rolesService:RolesService,
              private usuariosService:UsuariosService) { 
                this.usuarioDTO= new UsuarioDTO(null,null,null,null,null);
              }

  ngOnInit() {
     this.obtenerQueryParams();
     this.locStorage.getData();
     this.obtenerUsuarios();
  }

   obtenerQueryParams(){
        let seb= this.route.queryParams.subscribe(params => {

            // Defaults to 0 if no query param provided.
            this.id_aplicacion = +params['app'] || null;
        });

        if(this.id_aplicacion==null){
            alert("Faltan Mas datos de la Aplicación");
            this.aPerfil();
        }else{
                
                // this.loadData(); 
        
        }

  }

   aPerfil() {
        let link = ['/perfil'];
        this.router.navigate(link);
    }




openDialog(editable:boolean,usuario:Usuario|null) {
    let dialogRef = this.dialog.open(UsuarioNuevoComponent,{
        data:{
            editable:editable,
            usuario:usuario
        }

    });
    dialogRef.afterClosed().subscribe(result => {
      
      console.log(result)
     
      if(result!='cerrar'){
          if(result=='OK'){
                this.obtenerUsuarios();
          }else{

          }
          

         // 
        }
    });
  }

        // llama el metodo GET del servicio para obtener los datos
    obtenerUsuarios(): void {

        this.usuariosService.getUsuarios(this.id_aplicacion,null,null,null)
        .subscribe((data: Usuario[]) => this.usuarios = data,
          error => console.log(error),
                () => {
                  console.log(this.usuarios);
                  console.log('Get all Items complete');
              });
    }



}
