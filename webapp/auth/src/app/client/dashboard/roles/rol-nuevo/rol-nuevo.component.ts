import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params }   from '@angular/router';
import { FormGroup, FormBuilder , Validators } from '@angular/forms';
import { Location } from '@angular/common';
import {MdDialog, MdDialogRef} from '@angular/material';


/********************************
 *          Modelos 
 ********************************/

/********************************
 *          Servicios 
 ********************************/


@Component({
  moduleId:module.id,
  selector: 'app-rol-nuevo',
  templateUrl: './rol-nuevo.component.html',
  styleUrls: ['./rol-nuevo.component.scss']
})
export class RolNuevoComponent implements OnInit {

  action = 'Crear';
   title = 'Registrar un Nuevo Rol';
   form: FormGroup;

   

  constructor(public dialogRef: MdDialogRef<RolNuevoComponent>,
  public fb: FormBuilder,) {
     this.controlsCreate();
  }


  ngOnInit() {
  }

          // start controls from from
  public controlsCreate() {
    this.form = this.fb.group({
       rol: ['', Validators.compose([
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(50),
            Validators.pattern('[a-zA-Z]{1}[a-z A-Z0-9-]{0,48}')
          ])],
      
      descripcion: ['', Validators.compose([
             Validators.required,
            Validators.minLength(3),
            Validators.maxLength(100),
            Validators.pattern('[a-zA-Z]{1}[a-z A-Z0-9-(,).]{0,98}')
          ])]
    }); 

  }

}
