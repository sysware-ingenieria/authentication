
export class Aplicacion{

    
    id_aplicacion:number;
    nombre:string;
    descripcion:string;
    version:string;
    fecha_creacion:string;
    fecha_actualizacion:string;
    key_aplicacion:string;
    key_servidor:string;
   

    constructor(
        id_aplicacion:number,
        nombre:string,
        descripcion:string,
        version:string,
        fecha_creacion:string,
        fecha_actualizacion:string,
        key_aplicacion:string,
        key_servidor:string,
    ){}
}