import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params }   from '@angular/router';
import { FormGroup, FormBuilder , Validators } from '@angular/forms';
import { Location } from '@angular/common';


import { LocalStorage }  from '../../../../shared/localStorage';

import { AplicacionDTO } from '../../../profile/aplicacion/aplicacionDTO';
import { Aplicacion } from '../aplicacion';


import { AplicacionSevice } from '../../../profile/aplicacion/aplicaiones.service';
@Component({
  selector: 'app-detalle-app',
  templateUrl: './detalle-app.component.html',
  styleUrls: ['./detalle-app.component.scss']
})
export class DetalleAppComponent implements OnInit {

     
      form: FormGroup;
      aplicacionDTO:AplicacionDTO;
      aplicaciones:Aplicacion[];
      aplicacion:Aplicacion;
      id_aplicacion=null;
      id_usuario_app=null;
      action = 'Cambiar';
      cambioApp = false;
      esVisible = true;
      title = 'Registrar una Nueva Aplicación';
      

  constructor(public fb: FormBuilder,
              public locStorage: LocalStorage,
               private route: ActivatedRoute,
              public router: Router,
              public location: Location,
              private aplicacionSevice:AplicacionSevice) {
                this.aplicacion=new Aplicacion(null,null,null,null,null,null,null,null);
                 this.controlsCreate();
                    this.changeMarca();

                this.id_usuario_app=this.locStorage.getIdUsuarioApp();

                 this.aplicacionDTO=new AplicacionDTO(null,null,null);
               }

  ngOnInit() {
    let seb= this.route.queryParams.subscribe(params => {

        // Defaults to 0 if no query param provided.
        this.id_aplicacion = +params['app'] || null;
      });

      if(this.id_aplicacion==null){
          alert("Faltan Mas datos de la Aplicación");
          this.aPerfil();
      }else{

              this.obtenerAplicacion();
              
            // this.loadData(); 
      }

    

    
  }

    changeMarca() {
     this.form.valueChanges.subscribe((value) => {
         if(this.aplicacion.nombre!=null && ( this.aplicacion.nombre!=value['nombre'])){
              if(value['nombre']){
                this.cambioApp=true;
              }  
         }else if(this.aplicacion.descripcion!=null &&( this.aplicacion.descripcion!=value['descripcion'])){
                  if(value['descripcion']){
                    this.cambioApp=true;
                   }  
              }else if(this.aplicacion.version!=null &&( this.aplicacion.version!=value['version'])){
                  if(value['version']){
                    this.cambioApp=true;
                   }  
              }else{
                    this.cambioApp=false;
              }
    });

  }

           // start controls from from
  public controlsCreate() {
    this.form = this.fb.group({
       nombre: ['', Validators.compose([
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(50),
            Validators.pattern('[a-zA-Z]{1}[a-z A-Z0-9-]{0,48}')
          ])],
      
      descripcion: ['', Validators.compose([
             Validators.required,
            Validators.minLength(3),
            Validators.maxLength(100),
            Validators.pattern('[a-zA-Z]{1}[a-z A-Z0-9-(,).]{0,98}')
          ])],
    version: ['', Validators.compose([
             Validators.required,
            Validators.minLength(1),
            Validators.maxLength(10),
            Validators.pattern('[a-zA-Z]{1}[a-z A-Z0-9-(,).]{0,9}')
          ])],
    }); 

  }

    public loadData() {
      this.form.patchValue({
          nombre:this.aplicaciones[0].nombre,
          descripcion:this.aplicaciones[0].descripcion,
          version:this.aplicaciones[0].version,
      });
    

  }

     guardar(){
       

        
      this.aplicacionDTO.nombre = this.form.value['nombre'];
      this.aplicacionDTO.descripcion = this.form.value['descripcion'];
      this.aplicacionDTO.version = this.form.value['version'];
     

          this.aplicacionSevice.patchAplicaion(this.aplicacion.id_aplicacion,this.aplicacionDTO)
        .subscribe(  result => {
                  if ( result.json() ) {
                   alert('Cambios Guardados Satisfactoriamente! ');
                      //this.volver();
                     this.cambioApp = false;
                     this.obtenerAplicacion();
                      
                  }else {
                      alert('Error al  crear la Aplicación');
                       
                  }
                  
                });
    
  }

     // llama el metodo GET del servicio para obtener los datos
    // llama el metodo GET del servicio para obtener los datos
    obtenerAplicacion(): void {
       
        this.aplicacionSevice.getAplicaciones(this.id_aplicacion,this.id_usuario_app)
        .subscribe((data: Aplicacion[]) => this.aplicaciones = data,
          error => console.log(error),
                () => {
                  this.aplicacion=this.aplicaciones[0];
                  
                  this.loadData();
                  
              });
    }

 aPerfil() {
    let link = ['/perfil'];
    this.router.navigate(link);
  }

}
