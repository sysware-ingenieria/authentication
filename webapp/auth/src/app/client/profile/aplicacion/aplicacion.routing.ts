import { Routes } from '@angular/router';

import { AplicacionComponent } from './aplicacion/aplicacion.component';


export const OfertaRouting: Routes = [
    {
    path: 'aplicacion',
    component: null,
    children: [
      {path: '', redirectTo: 'lista', pathMatch: 'full'},
      {path: 'lista', component: AplicacionComponent},
    ]
  }
];
