import { Component, OnInit } from '@angular/core';
import { Router }   from '@angular/router';

import { LocalStorage }  from '../../../../shared/localStorage';

import { Aplicacion } from '../aplicacion';
import { AplicacionDTO } from '../aplicacionDTO';

import { AplicacionSevice } from '../aplicaiones.service';



@Component({
  selector: 'app-aplicacion',
  templateUrl: './aplicacion.component.html',
  styleUrls: ['./aplicacion.component.css']
})
export class AplicacionComponent implements OnInit {
  aplicaciones:Aplicacion[];



  constructor(private aplicacionService:AplicacionSevice, private router: Router,public locStorage: LocalStorage) {

        if  (this.locStorage.getData()===null){
                            alert("Debe Iniciar sesión")
                            this.router.navigate(['/auth']);

                        }else{
                          this.locStorage.getData();
                        } 
   
    
  }

    ngOnInit() {
      this.obtenerOfertas();
    }

    // llama el metodo GET del servicio para obtener los datos
    // llama el metodo GET del servicio para obtener los datos
    obtenerOfertas(): void {
      

        this.aplicacionService.getAplicaciones(null,this.locStorage.getIdUsuarioApp(),null,null,null,null,null,null,null)
        .subscribe((data: Aplicacion[]) => this.aplicaciones = data,
          error => console.log(error),
                () => {
                  console.log(this.aplicaciones);
                  console.log('Get all Items complete');
              });
    }


    verDetalle(id_oferta){
     
      let link = ['/dashboard/oferta/detalle',id_oferta];
       this.router.navigate(link);


    }
    borrar(){
      alert("Borrar");
      
    }

     crear(){
       
         this.router.navigate(['/dashboard/oferta/nueva']);
      
    }

  

}
