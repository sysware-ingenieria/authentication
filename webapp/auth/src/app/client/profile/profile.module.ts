import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';

/**
 *  Principal
 */
import { PerfilComponent,DialogResultExampleDialog } from './perfil/perfil.component';

import { AplicacionComponent } from './aplicacion/aplicacion/aplicacion.component';
import { AplicacionSevice } from './aplicacion/aplicaiones.service';



/***************************************************************
 * Llamar a todos los Modulos *
 ***************************************************************/
import { AplicacionModule }  from './aplicacion/aplicacion.module';


@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    MaterialModule,
    ReactiveFormsModule,
    

  
    ],
  declarations: [
    PerfilComponent,
    AplicacionComponent,
    DialogResultExampleDialog
    
    ],
     
  bootstrap: [PerfilComponent, DialogResultExampleDialog],
  exports: [ PerfilComponent,DialogResultExampleDialog ],
  providers:[AplicacionSevice]
})
export class ProfileModule { }


