import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params }   from '@angular/router';
import { FormGroup, FormBuilder , Validators } from '@angular/forms';
import { Location } from '@angular/common';

@Component({
    moduleId: module.id,
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
    isActive = false;
    showMenu = '';
     id_aplicacion=null;
    eventCalled() {
        this.isActive = !this.isActive;
    }
    addExpandClass(element: any) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        } else {
            this.showMenu = element;
        }
    }
    constructor(  private route: ActivatedRoute,
              public router: Router){

    }

    ngOnInit(){
            let seb= this.route.queryParams.subscribe(params => {

            // Defaults to 0 if no query param provided.
            this.id_aplicacion = +params['app'] || null;
        });

        if(this.id_aplicacion==null){
            alert("Faltan Mas datos de la Aplicación");
            this.aPerfil();
        }else{
                
                // this.loadData(); 
        
        }
    }

    aPerfil() {
        let link = ['/perfil'];
        this.router.navigate(link);
    }
}
