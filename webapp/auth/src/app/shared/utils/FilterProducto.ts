
export class FilterProducto{

        id_producto:number;
        nombre_producto:string;
        id_marca:number;
        nombre_marca:string;
        id_categoria:number;
        nombre_categoria:string;

    constructor(
        id_producto:number,
        nombre_producto:string,
        id_marca:number,
        nombre_marca:string,
        id_categoria:number,
        nombre_categoria:string
    ){}

}
