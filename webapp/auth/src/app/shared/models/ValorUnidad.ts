import { Unidad } from './Unidad';
export class ValorUnidad{

    id_valor_unidad:number;
    id_producto:number;
    unidad:Unidad;
    valor:number;
   

    constructor(
        id_valor_unidad:number,
        id_producto:number,
        unidad:Unidad,
        valor:number
    ){}
}
