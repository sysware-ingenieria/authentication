import { Injectable } from '@angular/core';
import { Http, Headers, Response, URLSearchParams, RequestOptions} from '@angular/http';


import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';

/** Files for auth process  */
import { urlbase } from '../shared/url';
import { LocalStorage } from '../shared/localStorage';
import { Auth } from './auth';
import { Token } from '../shared/token';

@Injectable()
export class AuthService {
  username: string;
  loggedIn: boolean;
  token: Token;
  urlAuth= urlbase[0] + '/auth';
  private options: RequestOptions;
  private headers = new Headers({'Content-Type': 'application/json'});


  constructor(private http: Http, private locStorage: LocalStorage) {

  }
 public login= (auth: Auth): Observable<Boolean> => {
    return this.http.post(this.urlAuth+'/login', auth , { headers: this.headers })
                .map((response: Response) => {

                  let token = response.json();
                  if ( token ) {
                    this.token = token;
                    this.loggedIn = true;
                    localStorage.setItem('currentUserApp3', JSON.stringify(this.token));
                    return true;
                  }else {
                    this.loggedIn = false;
                    return false;
                  }
                })
                .catch(this.handleError);
  }

   public register= (auth: Auth): Observable<Boolean> => {
        return this.http.post(this.urlAuth, auth , { headers: this.headers })
                .map((response: Response) => {

                  let token = response.json();
                  if ( token ) {
                
                    return true;
                  }else {
                    
                    return false;
                  }
                })
                .catch(this.handleError);
  }



  public logout= (): Observable<Boolean> => {
      if ( this.locStorage.isSession()){
          let headers = new Headers({
          'Content-Type' : 'application/json',
          'Authorization' :  this.locStorage.getTokenValue()
              });
            this.options = new RequestOptions({headers});
            let params: URLSearchParams = new URLSearchParams();
            params.set('id_user', '' + this.locStorage.getIdUsuarioApp());
            params.set('key_token', this.locStorage.getTokenValue());
            this.options.search = params;
          return this.http.delete(this.urlAuth, this.options)
            .map((response: Response) => {
                  this.locStorage.cleanSession();
                  return response;
            })
            .catch(this.handleError);
        }

  }

  isLoggedIn() {
    return this.loggedIn;
  }

  private handleError (error: Response | any): Observable<Boolean> {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;

    } else {
      errMsg = error.message ? error.message : error.toString();
    }
      //alert(errMsg);
      return Observable.throw(false);
    //return Observable.throw(errMsg);
  }






}